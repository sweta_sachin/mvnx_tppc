package SubscriberTests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class SubscriberServicesTests  extends Common_Methods.CommonMethods {
	
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
	       	
	}
	
	@AfterClass(alwaysRun=true)
	public void end()
	{
		driver.close();
	}
	
	@Test
	public void a_View_SubscriberServicesTab() throws InterruptedException
	{
		view_subscriberServicesTab();
	}
	
	@Test
	public void b_Click_SubscriberServicesTab() throws InterruptedException
	{
		Click_subscriberServicesTab();
	}
	
	@Test
	public void c_View_SubscriberServicesDetails() throws InterruptedException
	{
		view_subServiceDetails();
	}
	
	
	@Test
	public void d_View_AddRemoveServiceButton() throws InterruptedException
	{
		view_AddRemoveserviceButton();
	}
	
	@Test
	public void e_Click_AddRemoveServiceButton() throws InterruptedException
	{
		Thread.sleep(1200);
		click_AddRemoveServiceButton();
		Thread.sleep(1200);
		driver.navigate().back();
	}
	

	@Test
	public void f_View_FutureActionsButton() throws InterruptedException
	{
		view_futureActionButton();
	}
	
	@Test
	public void g_Click_FutureActionsButton() throws InterruptedException
	{
		Thread.sleep(1200);
		click_futureActionButton();
	}
	
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//****************************** Method to view Subscriber Services tab ***************************
public void view_subscriberServicesTab()
{
	WebElement SubServicesTab= driver.findElement(By.linkText("Subscriber Services"));

	Boolean SubServiceTab= SubServicesTab.isDisplayed();

	Assert.assertTrue(SubServiceTab);

}
//******************************** Method to click subscriber Services tab ***************************
public void Click_subscriberServicesTab()
{
	WebElement SubServicesTab= driver.findElement(By.linkText("Subscriber Services"));

	Actions act= new Actions(driver);

	act.moveToElement(SubServicesTab).click().build().perform();

}

//****************************** Method to view Subscriber service details  ***************************
public void view_subServiceDetails()
{

	WebElement service= driver.findElement(By.id("R28357401521988031"));

	Boolean ServiceDetails= service.isDisplayed();

	Assert.assertTrue(ServiceDetails);
}

//****************************** Method to view future Actions button  ***************************
public void view_futureActionButton()
{

	WebElement FutureAction= driver.findElement(By.id("B93429819029287287"));

	Boolean Button= FutureAction.isDisplayed();

	Assert.assertTrue(Button);

}

//****************************** Method to view future Actions button  ***************************
public void click_futureActionButton()
{
	driver.findElement(By.id("B93429819029287287")).click();

	WebElement element= driver.findElement(By.id("B93462910201022618"));

	Boolean verify= element.isDisplayed();

	Assert.assertTrue(verify);

}

//****************************** Method to view Add/Remove service button  ***************************
public void view_AddRemoveserviceButton()
{

	WebElement FutureAction= driver.findElement(By.id("B29011503255168026"));

	Boolean Button= FutureAction.isDisplayed();

	Assert.assertTrue(Button);

}

//****************************** Method to click add/remove service button  ***************************
public void click_AddRemoveServiceButton()
{
	driver.findElement(By.id("B29011503255168026")).click();

	WebElement element= driver.findElement(By.id("B48333112941434881"));

	Boolean verify= element.isDisplayed();

	Assert.assertTrue(verify);

  }
}
