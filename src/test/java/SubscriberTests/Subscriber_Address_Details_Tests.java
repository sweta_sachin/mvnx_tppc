package SubscriberTests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Subscriber_Address_Details_Tests extends Common_Methods.CommonMethods {
	
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
	       	
	}
	
	@AfterClass(alwaysRun=true)
	public void end()
	{
		driver.close();
	}
	
	@Test
	public void a_View_SubscriberAddressDetailsTab() throws InterruptedException
	{
		view_subscriberAddressDetailsTab();
	}
	
	@Test
	public void b_click_SubscriberAddressDetailsTab() throws InterruptedException
	{
		Click_subscriberAddressDetailsTab();
	}
	
	@Test
	public void c_View_SubscriberAddressDetails() throws InterruptedException
	{
		view_subscriberAddressDetails();
	}
	
	@Test
	public void d_View_FutureActionsButton() throws InterruptedException
	{
		view_futureActionButton();
	}
	
	@Test
	public void e_Click_FutureActionsButton() throws InterruptedException
	{
		Thread.sleep(1200);
		click_futureActionButton();
	}
	
	
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//****************************** Method to view Subscriber address details tab ***************************
public void view_subscriberAddressDetailsTab()
{
   WebElement SubAddDetail= driver.findElement(By.linkText("Subscriber Address Details"));
   
   Boolean addDetailsTab= SubAddDetail.isDisplayed();
   
   Assert.assertTrue(addDetailsTab);
   
}
//******************************** Method to click subscriber address details tab ***************************
public void Click_subscriberAddressDetailsTab()
{
	WebElement SubAddDetail= driver.findElement(By.linkText("Subscriber Address Details"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(SubAddDetail).click().build().perform();
	   
}

//****************************** Method to view Subscriber address details  ***************************
public void view_subscriberAddressDetails()
{
	WebElement SubAddDetail= driver.findElement(By.id("B93429819029287287"));
 
	Boolean addDetails= SubAddDetail.isDisplayed();
 
	Assert.assertTrue(addDetails);
 
}

//****************************** Method to view future Actions button  ***************************
public void view_futureActionButton()
{
 
	WebElement FutureAction= driver.findElement(By.id("B93429819029287287"));
 
	Boolean Button= FutureAction.isDisplayed();
 
	Assert.assertTrue(Button);
 
}

//****************************** Method to view future Actions button  ***************************
public void click_futureActionButton()
{
	
	driver.findElement(By.id("B93429819029287287")).click();

	WebElement element= driver.findElement(By.id("B93462910201022618"));

	Boolean verify= element.isDisplayed();

	Assert.assertTrue(verify);

  }
}
