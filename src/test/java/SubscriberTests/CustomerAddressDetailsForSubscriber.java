package SubscriberTests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CustomerAddressDetailsForSubscriber extends Common_Methods.CommonMethods {
	
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
	       	
	}
	
	@AfterClass(alwaysRun=true)
	public void end()
	{
		driver.close();
	}
	
	@Test
	public void a_View_CustomerDetailsTab() throws InterruptedException
	{
		view_customerAddDetailsTab();
	}
	
	@Test
	public void b_Click_CustomerAddressDetailsTab() throws InterruptedException
	{
		Click_CustomerAddDetailsTab();
	}
	
	@Test
	public void c_View_CustomerAddressDetails() throws InterruptedException
	{
		view_CustomerAddDetails();
	}
	

	
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//****************************** Method to view customer address details tab for associated subscriber  ***************************
public void view_customerAddDetailsTab()
{
	WebElement AddDetailsTab= driver.findElement(By.linkText("Customer Address Details"));

	Boolean Tab= AddDetailsTab.isDisplayed();

	Assert.assertTrue(Tab);

}

//******************************** Method to click customer address details tab for associated subscriber ***************************
public void Click_CustomerAddDetailsTab()
{
	WebElement AddDetailsTab= driver.findElement(By.linkText("Customer Address Details"));

	Actions act= new Actions(driver);

	act.moveToElement(AddDetailsTab).click().build().perform();

}

//****************************** Method to view customer address details for associated subscriber  ***************************
public void view_CustomerAddDetails()
{

	WebElement address= driver.findElement(By.id("R16581551875833684"));

	Boolean Details= address.isDisplayed();

	Assert.assertTrue(Details);
   }
}

