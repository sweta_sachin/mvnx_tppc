package SubscriberTests;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class SIM_swap_tests extends Common_Methods.CommonMethods {
		
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
	}
	
	@AfterClass(alwaysRun=true)
	public void end()
	{
    	driver.close();
	}
	
	@Test
	public void a_Click_SIM_Swap_ForSubscriber() throws InterruptedException
	{
		Click_SIMswap();
	}
	
	@Test
	public void b_Enter_New_ICCID_ForSubscriber() throws InterruptedException
	{
		EnterICCID();
	}
	
	@Test
	public void c_Click_Submit_ForSIMswap_ForSubscriber() throws InterruptedException
	{
		ClickSubmit();
	}
	
	@Test
	public void d_Check_SuccessMsg_ForSIMSWAP_ForSubscriber() throws InterruptedException
	{
		SIM_Swap_success();
	}
	
	@Test
	public void e_CheckRCSStatus() throws InterruptedException
	{
		RCSStatus_ForSIMSwap();
	}
	
	
	
/////////////////////////////////////////////////////////////////////////////////////////////////
//******************************** Method to click SIM swap for a subscriber ***************************
public void Click_SIMswap() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement SIMSWAP= driver.findElement(By.id("B13992068504418949"));
	
	Boolean name= SIMSWAP.isDisplayed();
	
	if(name==true)   {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(SIMSWAP).click().build().perform();
		
		Thread.sleep(1500);
		}
	}
		
//******************************* Method to enter new ICCID into the textbox******************************
public void EnterICCID() throws InterruptedException
{
	driver.findElement(By.id("P46_ICCID")).clear();
	
	driver.findElement(By.id("P46_ICCID")).sendKeys(ICCID);
	
	Thread.sleep(1500);
	
}
	
//******************************* Method to click submit ****************************************************
public void ClickSubmit() throws InterruptedException
{
	driver.findElement(By.id("B14237467074016054")).click();
	
	Thread.sleep(1500);
	
}

//******************************* Method to check success message after SIM swap is done ********************
public void SIM_Swap_success() throws InterruptedException
{
	Boolean SIMSWap= driver.findElement(By.xpath("//div[@id='active-notifications']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']")).isDisplayed();	
	
	Assert.assertTrue(SIMSWap);
	
	Thread.sleep(1500);
	
}

//******************************* Method to check success message after SIM swap is done ********************
public void RCSStatus_ForSIMSwap() throws InterruptedException
{
	//Click RCS tab
	
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);
			
	WebElement RCS= driver.findElement(By.id("B16614757006394351"));
	
	Boolean name= RCS.isDisplayed();
	
	if(name==true)  {
		  
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(RCS).click().build().perform();
		
		Thread.sleep(1500);
		
		String RCS_Column = driver.findElement(By.xpath("//tr[2]//td[2]")).getText();
			
		if(RCS_Column=="Perform a SIM Swap"){
				
			String Status= driver.findElement(By.xpath("//tr[2]//td[10]")).getText();
				
			Assert.assertEquals(Status, "Success");
		}
   	} 
  }
}
