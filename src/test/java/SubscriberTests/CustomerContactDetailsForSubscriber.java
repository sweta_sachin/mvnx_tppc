package SubscriberTests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CustomerContactDetailsForSubscriber extends Common_Methods.CommonMethods {
	
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
	       	
	}
	
	@AfterClass(alwaysRun=true)
	public void end()
	{
		driver.close();
	}
	
	@Test
	public void a_View_CustomerContactDetailsTab() throws InterruptedException
	{
		view_customerContactDetailsTab();
	}
	
	@Test
	public void b_Click_CustomerContactDetailsTab() throws InterruptedException
	{
		Click_CustomerContactDetailsTab();
	}
	
	@Test
	public void c_View_CustomerContactDetails() throws InterruptedException
	{
		view_CustomerContactDetails();
	}
	

	
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//****************************** Method to view customer Contact details tab for associated subscriber  ***************************
public void view_customerContactDetailsTab()
{
	WebElement ContactDetailsTab= driver.findElement(By.linkText("Customer Contact Details"));

	Boolean Tab=ContactDetailsTab.isDisplayed();

	Assert.assertTrue(Tab);

}
//******************************** Method to click customer Contact details tab for associated subscriber ***************************
public void Click_CustomerContactDetailsTab()
{
	WebElement ContactDetailsTab= driver.findElement(By.linkText("Customer Contact Details"));

	Actions act= new Actions(driver);

	act.moveToElement(ContactDetailsTab).click().build().perform();

}

//****************************** Method to view customer Contact details for associated subscriber  ***************************
public void view_CustomerContactDetails()
{

	WebElement Contact= driver.findElement(By.id("R35036010409414053"));

	Boolean Details= Contact.isDisplayed();

	Assert.assertTrue(Details);
   } 
}
