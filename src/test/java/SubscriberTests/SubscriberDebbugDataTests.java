package SubscriberTests;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class SubscriberDebbugDataTests extends Common_Methods.CommonMethods {
	
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
	}
	
	@AfterClass(alwaysRun=true)
	public void end()
	{
		driver.close();
	}
	
	@Test
	public void Click_Subscriber_DebugData() throws InterruptedException
	{
		Click_debugdata();
	}
	
	@Test
	public void view_Subscriber_DebugData() throws InterruptedException
	{
		view_debugdata();
	}
	
	
/////////////////////////////////////////////////////////////////////////////////////////////////
//******************************** Method to click bill limit history ***************************
public void Click_debugdata() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement attachment= driver.findElement(By.id("B32342205552931356"));
	
	Boolean name= attachment.isDisplayed();
	
	if(name==true)   {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(attachment).doubleClick().build().perform();
		
		Thread.sleep(1500);
	}
}

//****************************** View debug data ***************************************
public void view_debugdata() throws InterruptedException
{
	WebElement table= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[2]/div[1]"));
	
	Boolean report= table.isDisplayed();
	
	Assert.assertTrue(report);
	
   }
}

