package SubscriberTests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class CustomerDetailsOfAssociatedSubscriber extends Common_Methods.CommonMethods {
	
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
	       	
	}
	
	@AfterClass(alwaysRun=true)
	public void end()
	{
		driver.close();
	}
	
	@Test
	public void a_View_CustomerDetailsTab() throws InterruptedException
	{
		view_customerDetailsTab();
	}
	
	@Test
	public void b_Click_CustomerDetailsTab() throws InterruptedException
	{
		Click_CustomerDetailsTab();
	}
	
	@Test
	public void c_View_CustomerDetails() throws InterruptedException
	{
		view_CustomerDetails();
	}
	

	
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//****************************** Method to view customer details tab for associated subscriber  ***************************
public void view_customerDetailsTab()
{

	WebElement SubServicesTab= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[2]/ul[1]/li[4]/a[1]"));

	Boolean SubServiceTab= SubServicesTab.isDisplayed();

	Assert.assertTrue(SubServiceTab);

}

//******************************** Method to click customer details tab for associated subscriber ***************************
public void Click_CustomerDetailsTab()
{

	WebElement SubServicesTab= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[2]/ul[1]/li[4]/a[1]"));

	Actions act= new Actions(driver);

	act.moveToElement(SubServicesTab).click().build().perform();

}

//****************************** Method to view customer details for associated subscriber  ***************************
public void view_CustomerDetails()
{

	WebElement service= driver.findElement(By.id("R13616970911260349"));

	Boolean ServiceDetails= service.isDisplayed();

	Assert.assertTrue(ServiceDetails);
   }
}
