package CRM_Tab;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class AccessTabsOnCRM extends Common_Methods.CommonMethods{
	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		SearchCustomerByAccountNumber(AccNo);
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
	  driver.quit();
		
	}
	
	@Test
	public void a_AccessEditCustomerTab() throws InterruptedException
	{
		Thread.sleep(1000);
		ClickEdit_Customer();
		Thread.sleep(1000);
		driver.navigate().back();
	}
 
	@Test
	public void b_AccessCreateSubcriberTab() throws InterruptedException
	{
		Thread.sleep(1500);
		AccessCreateSubcriber();
		driver.navigate().back();
		
	}
	
	@Test
	public void c_AccessQuickNotesTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessQuickNotes();
	
	}
	
	@Test
	public void d_AccessAttachmentsTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessAttachments();
		
	}
	
	@Test
	public void e_AccessCommunicationTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessCommunication();
		
	}
	
	@Test
	public void f_AccessNotificationTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessNotification();
		
	}
	
	@Test
	public void g_AccessCustomerDebugDataTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessCustomerDebugData();
		
	}
	
	@Test
	public void h_AccessDirectDebitTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessDirectDebit();
		
	}
	
	@Test
	public void i_AccessViewInvoicesTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessViewInvoices();
		
	}
	
	@Test
	public void j_AccessDiscountTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessDiscount();
		
	}
	
	@Test
	public void k_AccessAd_hocCHargesTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessAd_hocCharges();
		
	}
	
	@Test
	public void l_Accessinstant_billTab() throws InterruptedException
	{
		Thread.sleep(1000);
		Accessinstant_bill();
		
	}
	
	 @Test
	public void m_AccessDeactivateTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessDeactivate();
		
	}  
	
	@Test
	public void n_AccessAccount_chargesTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessAccount_charges();
		
	}
	
	@Test
	public void o_AccessFinacialAdjustmentTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessFinacialAdjustment();
		
	}
	
	@Test
	public void p_AccessAutoBarHistoryTab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessAutoBarHistory();
		
	}
	
	@Test
	public void q_AccessNotificationExclusion_Tab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessNotificationExclusion();
		
	}
	
	@Test
	public void r_AccessSponsors_Tab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessSponsors();
		
	}
	
	@Test
	public void s_AccessCustomerHistory_Tab() throws InterruptedException
	{
		Thread.sleep(1000);
		AccessCustomerHistory();
		
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//**********************Method to test Access Quick notes ****************************************************	
public void AccessQuickNotes() throws InterruptedException
{
	try {
	
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);
			
		WebElement QuickNotes= driver.findElement(By.id("B13528763358177637"));
		
		Boolean name= QuickNotes.isDisplayed();
		
		if(name==true)   {
		    
			wait.equals(name);
		    
			Actions act= new Actions(driver);
			
			act.moveToElement(QuickNotes).click().build().perform();
			
			Thread.sleep(500);
			
			driver.navigate().back();
		}
		
	}catch(StaleElementReferenceException e) {
	
		WebElement QuickNotes= driver.findElement(By.id("B13528763358177637"));
		
		Actions act1= new Actions(driver);
		
		act1.moveToElement(QuickNotes).click().build().perform();
		
	}
	
}
	
//**********************Method to test Access attachment****************************************************	
public void AccessAttachments() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement attachment= driver.findElement(By.id("B8981814074129539"));
	
	Boolean name= attachment.isDisplayed();
	
	if(name==true)  {
		  
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(attachment).doubleClick().build().perform();
		
		Thread.sleep(500);
		
		driver.navigate().back();
	}
}

//**********************Method to test Access communication****************************************************	
public void AccessCommunication() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement comms= driver.findElement(By.id("B43693003878117959"));
	
	Boolean com= comms.isDisplayed();
	
	if(com==true)  {
			    
		wait.equals(com);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(comms).click().build().perform();
		
		Thread.sleep(500);
		
		driver.navigate().back();
	}
}

//**********************Method to test Access notification****************************************************	
public void AccessNotification() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement notes= driver.findElement(By.id("B12119121652547560"));
	
	Boolean name= notes.isDisplayed();
	
	if(name==true)   {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(notes).doubleClick().build().perform();
		
		Thread.sleep(500);
		
		driver.navigate().back();
	}
}
	
//**********************Method to test Access Customer DebugData****************************************************	
public void AccessCustomerDebugData() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement notes= driver.findElement(By.id("B32340706460922145"));
	
	Boolean name= notes.isDisplayed();
	
	if(name==true)  {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(notes).doubleClick().build().perform();
		
		Thread.sleep(500);
		
		driver.navigate().back();
	}
}
	
//**********************Method to test Access Direct debit****************************************************		
public void AccessDirectDebit() throws InterruptedException
{  
	try {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);
	
		WebElement notes= driver.findElement(By.id("B62690930510264602"));
		
		Boolean name= notes.isDisplayed();
		
		if(name==true)  {
		   
			wait.equals(name);
		    
			Actions act= new Actions(driver);
			
			act.moveToElement(notes).doubleClick().build().perform();
			
			Thread.sleep(500);
			
			driver.navigate().back();
		    
		}
		
	}catch(NoSuchElementException  e)  {
	
		WebElement notes= driver.findElement(By.cssSelector("#B62690930510264602"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(notes).doubleClick().build().perform();
		
		Thread.sleep(500);
		
		driver.navigate().back();
	}
}

//**********************Method to test view invoices****************************************************		
public void AccessViewInvoices() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement notes= driver.findElement(By.id("B31575309173114291"));
	
	Boolean name= notes.isDisplayed();
	
	if(name==true)  {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(notes).doubleClick().build().perform();
		
		Thread.sleep(500);
		
		driver.navigate().back();
	}
}
	
//**********************Method to test Access Discounts****************************************************	
public void AccessDiscount() throws InterruptedException
{
	try {
	
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);
			
		WebElement notes= driver.findElement(By.id("B5018025327972823"));
		
		Boolean name= notes.isDisplayed();
		
		if(name==true) {
		    
			wait.equals(name);
		    
			Actions act= new Actions(driver);
			
			act.moveToElement(notes).doubleClick().build().perform();
			
			Thread.sleep(500);
			
			driver.navigate().back();
		}
	   	
	} catch(NoSuchElementException e)	{
	
		WebElement notes= driver.findElement(By.cssSelector("#B5018025327972823"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(notes).doubleClick().build().perform();
		
		Thread.sleep(500);
		
		driver.navigate().back();
	 }
}
	
//**********************Method to test Access ad-hoc charges****************************************************		
public void AccessAd_hocCharges() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement notes= driver.findElement(By.id("B50770423001886018"));
	
	Boolean name= notes.isDisplayed();
	
	if(name==true)   {
	
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(notes).doubleClick().build().perform();
		
		Thread.sleep(500);
	
		driver.navigate().back();
	}
}
	
//**********************Method to test Access Customer instant bill****************************************************	
public void Accessinstant_bill() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
				
	WebElement notes= driver.findElement(By.id("B43652226619858837"));
	
	Boolean name= notes.isDisplayed();
	
	if(name==true) {
	
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(notes).doubleClick().build().perform();
		
		Thread.sleep(500);
		
		driver.navigate().back();
	}
}
	
//**********************Method to test Access deactivate tab ****************************************************		
public void AccessDeactivate() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement notes= driver.findElement(By.linkText("Deactivate"));
	
	Boolean name= notes.isDisplayed();
	
	if(name==true) {
	
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(notes).doubleClick().build().perform();
		
		Thread.sleep(500);
		
		driver.navigate().back();
   }
}

//**********************Method to test Access account charges****************************************************	
public void AccessAccount_charges() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement notes= driver.findElement(By.id("B75658413721424833"));
	
	Boolean name= notes.isDisplayed();
	
	if(name==true) {
	
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(notes).doubleClick().build().perform();
		
		Thread.sleep(500);
		
		driver.navigate().back();
	}
}
	
//**********************Method to test Access Financial adjustment ****************************************************	
public void AccessFinacialAdjustment() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement notes= driver.findElement(By.id("B75722923293888088"));
	
	Boolean name= notes.isDisplayed();
	
	if(name==true) {
	
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(notes).doubleClick().build().perform();
		
		Thread.sleep(500);
		
		driver.navigate().back();
	}
}
	
//**********************Method to test Access Autobar history ****************************************************	
public void AccessAutoBarHistory() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement notes= driver.findElement(By.id("B76885014023393099"));
	
	Boolean name= notes.isDisplayed();
	
	if(name==true) {
	
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(notes).doubleClick().build().perform();
		
		Thread.sleep(500);
		
		driver.navigate().back();
	}
}


//**********************Method to test Access Notification exclusion****************************************************	
public void AccessNotificationExclusion() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement notes= driver.findElement(By.id("B78499006552606022"));
	
	Boolean name= notes.isDisplayed();
	
	if(name==true) {
	
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(notes).doubleClick().build().perform();
		
		Thread.sleep(500);
				
		driver.navigate().back();
	}
}
	
//**********************Method to test Access sponsors  ****************************************************	
public void AccessSponsors() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
				
	WebElement notes= driver.findElement(By.id("B91951123054272107"));
	
	Boolean name= notes.isDisplayed();
	
	if(name==true){
	
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(notes).doubleClick().build().perform();
		
		Thread.sleep(500);
		
		driver.navigate().back();
	}
}

//**********************Method to test Access Customer history ****************************************************	
public void AccessCustomerHistory() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement notes= driver.findElement(By.xpath("//span[@class='ui-icon ui-icon-circle-triangle-s']"));
	
	Boolean name= notes.isDisplayed();
	
	if(name==true)  {
	
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(notes).click().build().perform();
		
		Thread.sleep(500);
    }
  }
}
