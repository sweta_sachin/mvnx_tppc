package CRM_Tab;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class DirectDebit_tab_tests extends Common_Methods.CommonMethods{
	
	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		SearchCustomerByAccountNumber(AccNo);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	  Thread.sleep(1000);
		driver.quit();
		
	}
	
	@Test
	public void click_DirectDebit_tab() throws InterruptedException
	{
		click_directdebit();	
		
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//*********************************** Method to Click direct debit tab ***************************************
public void click_directdebit()
{
	try {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);

		WebElement notes= driver.findElement(By.id("B62690930510264602"));

		Boolean name= notes.isDisplayed();

		if(name==true){

			wait.equals(name);

			Actions act= new Actions(driver);

			act.moveToElement(notes).doubleClick().build().perform();
		}

	}catch(NoSuchElementException| StaleElementReferenceException e)  {

		WebElement notes= driver.findElement(By.cssSelector("#B62690930510264602"));

		Actions act= new Actions(driver);

		act.moveToElement(notes).doubleClick().build().perform();
      }
   }
}
