package CRM_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class All_Subscriber_tab extends Common_Methods.CommonMethods{
	
	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		SearchCustomerByAccountNumber(AccNo);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
	  driver.quit();
		
	}
	
	@Test
	public void a_View_AllSubscriber_tab() throws InterruptedException
	{
		 View_AllSubscriberTab();
	}

	@Test
	public void b_Access_AllSubscriber_tab() throws InterruptedException
	{
		Access_All_SubscriberDetailsTab();
	}
	
	@Test
	public void c_View_Subscriber_details() throws InterruptedException
	{
		view_SubscriberDetails();
	}

	
	
/////////////////////////////////////////////////////////////////////////////////////////////////////
//**************************** Method to view all subscriber details tab ******************************
public void View_AllSubscriberTab()
{
	WebElement allSub= driver.findElement(By.linkText("All Subscribers for this Account"));

	Boolean Adetails= allSub.isDisplayed();

	Assert.assertTrue(Adetails);
}

//*************************** Method to access address details tab ****************************
public void Access_All_SubscriberDetailsTab() throws InterruptedException
{
	Thread.sleep(1500);

	WebElement address= driver.findElement(By.linkText("All Subscribers for this Account"));

	Actions act= new Actions(driver);

	act.moveToElement(address).doubleClick().build().perform();
}

//************************** Method to view address details of customer **************************
public void view_SubscriberDetails() throws InterruptedException
{

	Thread.sleep(1500);

	WebElement Subscriber= driver.findElement(By.xpath("//div[@id='R13748948158072242']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));

	Boolean Sdetails= Subscriber.isDisplayed();

	Assert.assertTrue(Sdetails); 
   }
}
