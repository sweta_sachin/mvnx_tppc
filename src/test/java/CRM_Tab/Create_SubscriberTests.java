package CRM_Tab;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class Create_SubscriberTests extends Common_Methods.CommonMethods{
	
	public String MSISDN= "27650417821";
	
	public String SIM= "8927076191407308400";// This is nothing but ICCID
	
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		SearchCustomerByAccountNumber(AccNo);
		Thread.sleep(1500);
		AccessCreateSubcriber();
	}
	
	@AfterClass(alwaysRun=true)
	public void end() throws InterruptedException
	{
		Thread.sleep(1200);
		driver.close();
	}
	
	@Test
	public void a_Select_TarrifType_Tab() throws InterruptedException
	{
		Thread.sleep(500);
		Select_TarrifType();
	}
	@Test
	public void ab_Enter_name()
	{
		driver.findElement(By.id("P34_BSD_NAME")).clear();
		driver.findElement(By.id("P34_BSD_NAME")).sendKeys("First name");
	}
	
	@Test
	public void b_Select_Title_tab() throws InterruptedException
	{
		Thread.sleep(500); 
		Select_Title();
	}
	
	@Test
	public void c_Select_BillCycle_Tab() throws InterruptedException
	{
		Thread.sleep(500); 
		Select_BillCycle();
	}
	
	@Test
	public void ca_Entre_CRMRefNumber() throws InterruptedException
	{
		Thread.sleep(500); 
		driver.findElement(By.id("P34_BSD_EXTERNAL_REFERENCE")).sendKeys("123456781");
	}
	
	@Test
	public void d_Select_DeliveryAddress_Tab() throws InterruptedException
	{
		Thread.sleep(500); 
		Select_DeliveryAddress();
		Thread.sleep(500); 
		driver.findElement(By.id("P34_ADDRESS_LEFT_5")).clear();
		driver.findElement(By.id("P34_ADDRESS_LEFT_5")).sendKeys("1234");
	}
	
	@Test
	public void da_Select_ParentDealer_Tab() throws InterruptedException
	{
		Thread.sleep(800); 
		Select_ParentDealer();
	}
	
	@Test
	public void f_Select_Dealer_Tab() throws InterruptedException
	{
		Thread.sleep(800); 
		Select_Dealer();
	}
	
	@Test
	public void g_Select_plantype_Tab() throws InterruptedException
	{
		Thread.sleep(500); 
		Select_plantype();
	}
	
	@Test
	public void h_Select_RatePlan_Tab() throws InterruptedException
	{
		Thread.sleep(500); 
		Select_RatePlan();
	}
	
	@Test
	public void i_Select_MSISDN() throws InterruptedException
	{
		Thread.sleep(500); 
		select_MSISDN(MSISDN);
	}
	
	@Test
	public void j_Enter_SIM_Number() throws InterruptedException
	{
		Thread.sleep(500); 
		Enter_SIM_number(SIM);
	}
	
	@Test
	public void k_Select_Gender() throws InterruptedException
	{
		Thread.sleep(500); 
		Select_gender();
	}
	
	@Test
	public void l_Enter_LastName()
	{
		Enter_lastName();
	}
	
	@Test
	public void m_Enter_Passport_number()
	{
		Enter_passport_number();
	}
	
	@Test
	public void n_Select_NetworkType()
	{
		Select_networkType();
	}
	
	@Test
	public void z_Click_ApplyChanges()
	{
		Click_applyChanges();
	}
	
	@Test
	public void Check_Success_Msg() throws InterruptedException
	{
		Thread.sleep(3000);
		Check_success_Msg();
	}
	
	@Test
	public void e_CheckRCSStatus() throws InterruptedException
	{
		RCS_Status_CreateSubacriber();
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////
//**************************************Method to select tariff type*************************************
public void Select_TarrifType()
{
	//Click tariff type
	
	try {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);
	
		WebElement tariff= driver.findElement(By.id("P34_TARIFF_TYPE_1"));
		
		Boolean name= tariff.isDisplayed();
		
		wait.equals(name);
		
		tariff .click();
		
	}catch(NoSuchElementException e)		{
	
		WebElement tariff= driver.findElement(By.id("P34_TARIFF_TYPE_1"));
		
		tariff.click();
		}
	}
	
//**************************************Method to select title*************************************
public void Select_Title()
{
	//Select title
	
	Select dropdown= new Select(driver.findElement(By.id("P34_BSD_T_UID")));
	
	dropdown.selectByVisibleText("Mr");		
	
}
	
//**************************************Method to select bill cycle*************************************	
public void Select_BillCycle()
{
	// Select billCycle
	
	Select Billcycle= new Select(driver.findElement(By.id("P34_BSD_BBC_UID")));
	
	Billcycle.selectByIndex(1);
	
}
	
//**************************************Method to select delivery address*************************************	
public void Select_DeliveryAddress()
{
	// Select delivery address
	
	driver.findElement(By.id("P34_DELIVERY_ADDRESS_0")).click();
	
}

//**************************************Method to select parent dealer*************************************
public void Select_ParentDealer() throws InterruptedException
{
	//Select parent dealer
	
	Select parentDealer= new Select(driver.findElement(By.id("P34_PARENT_D_UID")));
	
	Thread.sleep(500);
	
	parentDealer.selectByVisibleText("TPPC");
	
}

//**************************************Method to select dealer*************************************
public void Select_Dealer() throws InterruptedException
{
	//Select  dealer
	
	Select Dealer= new Select(driver.findElement(By.id("P34_D_UID")));
	
	Thread.sleep(500);
	
	Dealer.selectByVisibleText("TPPC");
	
}
	
//**************************************Method to select plan type*************************************
public void Select_plantype()
{
	// Select plan type
	
	driver.findElement(By.id("P34_RP_DEAL_0")).click();
	
}
	
//**************************************Method to select rate plan*************************************
public void Select_RatePlan()
{
	//Select rate plan
	
	Select rateplan= new Select(driver.findElement(By.id("P34_RP_UID")));
	
	rateplan.selectByVisibleText("DefaultRatePlan");
	
}
	
//**************************************Method to select MSISDN *************************************
public void select_MSISDN(String MSISDN)
{
	WebElement msisdn= driver.findElement(By.id("P34_MSISDN"));
	
	msisdn.clear();
	
	msisdn.sendKeys(MSISDN);
	
}
	
//**************************************Method to Enter SIM number*************************************
public void Enter_SIM_number(String SIM) throws InterruptedException
{
	WebElement sim= driver.findElement(By.id("P34_ICCID"));
	
	sim.clear();
	
	Thread.sleep(2000);
	
	sim.sendKeys(SIM);
	
}
	
//**************************************Method to select gender*************************************
public void Select_gender()
{
	driver.findElement(By.id("P34_BSD_GENDER_0")).click();
	
}
	
//**************************************Method to enter last name*************************************
public void Enter_lastName()
{
	WebElement surname= driver.findElement(By.id("P34_BSD_SURNAME"));
	
	surname.sendKeys("Surname");
	
}
	
//**************************************Method to enter passport number*************************************
public void Enter_passport_number()
{
	WebElement passport= driver.findElement(By.id("P34_BSD_PASSPORT_NUMBER"));
	
	passport.sendKeys("M8074780");
	
}

//**************************************Method to select network type*************************************
public void Select_networkType()
{
	//Select network type
	
	Select networkType= new Select(driver.findElement(By.id("P34_BSD_BST_UID")));
	
	networkType.selectByIndex(1);
	
}

//**************************************Method to click apply changes *************************************
public void Click_applyChanges()
{
	driver.findElement(By.id("B17126421400896750")).click();
	
}


//**************************************Method to check success message *************************************
public void Check_success_Msg()
{
	WebElement msg= driver.findElement(By.xpath("//div[@id='echo-message']"));
		
	String message= msg.getText();
		
	System.out.println(message);
		
	Boolean MSG= msg.isDisplayed();
				
	Assert.assertFalse(MSG);
	}
	
//************************************** Method to check RCS status *************************
public void RCS_Status_CreateSubacriber() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement RCS= driver.findElement(By.id("B16614757006394351"));
	
	Boolean name= RCS.isDisplayed();
	
	if(name==true)   {
				   
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(RCS).click().build().perform();
		
		Thread.sleep(1500);
					
		String Status= driver.findElement(By.xpath("//tr[2]//td[10]")).getText();
				
		Assert.assertEquals(Status, "Success");
	} 
  }		
}
