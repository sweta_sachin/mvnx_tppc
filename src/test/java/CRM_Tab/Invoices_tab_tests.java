package CRM_Tab;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class Invoices_tab_tests extends Common_Methods.CommonMethods{
	
	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		SearchCustomerByAccountNumber(AccNo);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	  Thread.sleep(1000);
		driver.quit();
		
	}
	
	@Test
	public void click_Invoice_tab() throws InterruptedException
	{
		click_invoicetab();
	}
		
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************************* Method to click invoices tab **************************
public void click_invoicetab()
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);

	WebElement notes= driver.findElement(By.id("B31575309173114291"));

	Boolean name= notes.isDisplayed();

	if(name==true){

		wait.equals(name);

		Actions act= new Actions(driver);

		act.moveToElement(notes).doubleClick().build().perform();
       }
    }
}
