package CRM_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class Customer_Address_Details extends Common_Methods.CommonMethods{
	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		SearchCustomerByAccountNumber(AccNo);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
	  driver.quit();
		
	}
	
	@Test
	public void a_View_address_details_tab() throws InterruptedException
	{
		View_AddressDetailsTab();
	}

	@Test
	public void b_Access_address_details_tab() throws InterruptedException
	{
		Access_AddressDetailsTab();
	}
	
	@Test
	public void c_View_Address_details() throws InterruptedException
	{
		view_addressDetails();
	}

	
	
/////////////////////////////////////////////////////////////////////////////////////////////////////
//**************************** Method to view address details tab ******************************
public void View_AddressDetailsTab()
{
	WebElement address= driver.findElement(By.xpath("//a[contains(text(),'Customer Address Details')]"));

	Boolean Adetails= address.isDisplayed();

	Assert.assertTrue(Adetails);
}

//*************************** Method to access address details tab ****************************
public void Access_AddressDetailsTab() throws InterruptedException
{

	Thread.sleep(1500);

	WebElement address= driver.findElement(By.xpath("//a[contains(text(),'Customer Address Details')]"));

	Actions act= new Actions(driver);

	act.moveToElement(address).doubleClick().build().perform();
}

//************************** Method to view address details of customer **************************
public void view_addressDetails() throws InterruptedException
{
	Thread.sleep(1500);

	WebElement address= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[2]/div[2]/div[1]/div[1]"));

	Boolean Adetails= address.isDisplayed();

	Assert.assertTrue(Adetails); 
  }
}
