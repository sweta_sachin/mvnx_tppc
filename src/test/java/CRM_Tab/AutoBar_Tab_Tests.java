package CRM_Tab;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class AutoBar_Tab_Tests extends Common_Methods.CommonMethods{
	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		SearchCustomerByAccountNumber(AccNo);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
	  driver.quit();
		
	}
	
	@Test
	public void a_click_Autobar_tab() throws InterruptedException
	{
		Click_autobar();
		
	} 
	
	@Test
	public void b_select_startDate() throws InterruptedException
	{
		StartDate();
	}
	
	@Test
	public void c_select_EndDate() throws InterruptedException
	{
		EndDate();
	}
	
	@Test
	public void d_SubmitSearchData()
	{
		click_submit();
	}
	
	@Test
	public void e_Cancel_autobar() throws InterruptedException
	{
		Cancel();
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************* Method to click autobar tab ***************************************
public void Click_autobar() throws InterruptedException {
try{
	
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
	    
	Thread.sleep(1500);
	
	WebElement notes= driver.findElement(By.id("B76885014023393099"));
	
	Boolean name= notes.isDisplayed();
	
	if(name==true) {
	
		wait.equals(name);
	    
		Actions act= new Actions(driver);
		
		act.moveToElement(notes).doubleClick().build().perform();
	}
	
}catch(StaleElementReferenceException | NoSuchElementException e) {

	Thread.sleep(1500);
	
	WebElement notes= driver.findElement(By.id("B76885014023393099"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(notes).doubleClick().build().perform();
	
  }
}

//*********************************** Method to select start date ******************************************
public void StartDate() throws InterruptedException
{
	
	// Click on the calendar
	
	Thread.sleep(500);
	
	WebElement datepicker= driver.findElement(By.xpath("//td[2]//img[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(datepicker).click().build().perform();
	
	WebElement month= driver.findElement(By.className("ui-datepicker-month"));
	
	Select dropdown= new Select(month);
	
	dropdown.selectByVisibleText("Jan");
	
	Thread.sleep(500);
	
	List<WebElement> date= driver.findElements(By.cssSelector(".ui-state-default"));
	
	int Size= date.size();
	
	for (int i=0; i<Size; i++)	{
		
		String text= date.get(i).getText();
		
		if(text.equalsIgnoreCase("23")) {
			
			date.get(i).click();
			
			break;
		  }
	    }
	}

//********************************* Method to select End date *********************************************
 public void EndDate() throws InterruptedException
 {
	 WebElement datepicker= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/table[1]/tbody[1]/tr[1]/td[4]/img[1]"));

	 Actions act= new Actions(driver);
	
	 act.moveToElement(datepicker).click().build().perform();
	
	 WebElement month= driver.findElement(By.className("ui-datepicker-month"));
	
	 Select dropdown= new Select(month);
	
	 dropdown.selectByVisibleText("Mar");
	
	 Thread.sleep(500);
	
	 List<WebElement> date= driver.findElements(By.cssSelector(".ui-state-default"));
	
	 int Size= date.size();
	
	 for (int i=0; i<Size; i++)		{
			
		 String text= date.get(i).getText();
		
		 if(text.equalsIgnoreCase("20"))	{
				
			 date.get(i).click();
			
			 break;
			}
		 }
     }
 
 //******************************* Method to click submit button *********************************
 public void click_submit()
 {

	 driver.findElement(By.id("P120_SUBMIT")).click();
 }

 //******************************* Method to cancel autobar search ******************************
 public void Cancel() throws InterruptedException
 {
	 try {
	
		 Thread.sleep(1000);
	 
		 driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[1]/div[2]/a[1]")).click();
	 
	 }catch(NoSuchElementException e) {
	
		 driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[1]/div[2]/a[1]")).click();
	 }
	 Thread.sleep(1500);
	 
	 WebElement edit= driver.findElement(By.linkText("Edit Customer"));
	 
	 Boolean Element = edit.isDisplayed();
	 
	 Assert.assertTrue(Element);
	 
   }
}
