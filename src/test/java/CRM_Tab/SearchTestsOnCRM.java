package CRM_Tab;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class SearchTestsOnCRM extends Common_Methods.CommonMethods {
	
	
	@BeforeClass
	public void start()
	{
		Login();
	}
	
	@AfterClass(alwaysRun=true)
	public void end()
	{
		driver.manage().timeouts().implicitlyWait(2,TimeUnit.SECONDS);
		driver.close();
	}

	@Test
	public void aViewCRMTab()
	{
		
		try	{
			Boolean CRM= driver.findElement(By.linkText("CRM")).isDisplayed();
			
			Assert.assertTrue(CRM);
		
		}catch( NoSuchElementException| StaleElementReferenceException e){
			
			Boolean CRM= driver.findElement(By.linkText("CRM")).isDisplayed();
			
			Assert.assertTrue(CRM);
		}
	}
	
	@Test
	public void aaAccessOrClickCRMTab()
	{
		Boolean CRM= driver.findElement(By.linkText("CRM")).isEnabled();
		
		if(CRM==true)	{
		
			driver.findElement(By.linkText("CRM")).submit();
		}
	}
	
	@Test
	public void bSearchByAccountNumberOnCRMTab()
	 {
		SearchCustomerByAccountNumber(AccNo);
		 
	   }
	
	@Test
	public void bbSearchByAccountNameOnCRMTab()
	{      
		Search_name( AccName);
		 
	   }
	
	
	@Test
	public void cSearchByAccountUIDOnCRMTab()
	{
	     Select dropdown = new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
		
	     dropdown.selectByVisibleText("Account UID");
		 
	     driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
		 
	     driver.findElement(By.id("P0_SEARCH_VALUE")).sendKeys(UID);
		 
	     driver.findElement(By.id("P0_SEARCH")).submit();
		 
	     WebElement CustomerName= driver.findElement(By.id("P2_BCD_NAME"));
		 
	     Boolean CustomerNameTab= CustomerName.isDisplayed();
		 
	     Assert.assertTrue(CustomerNameTab);
		 
	     driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
	   }

	
	@Test
	public void ccMSISDNnumberCRMTab()
	{    
	   try {
		
		   Select dropdown = new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
			
		   dropdown.selectByVisibleText("MSISDN");
			
		   driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
			
		   driver.findElement(By.id("P0_SEARCH_VALUE")).sendKeys(msisdn);
			
		   driver.findElement(By.id("P0_SEARCH")).submit();
			
		   WebElement MSISDN= driver.findElement(By.id("P2_SI_MSISDN"));
			
		   String MSIsdn= MSISDN.getText();
			
		   Assert.assertEquals(MSIsdn, msisdn);
			
		   driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
		  
	   } catch(StaleElementReferenceException E) {
		  
		   Select dropdown = new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
		  
		   dropdown.selectByVisibleText("MSISDN");
		  
		   driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
		  
		   driver.findElement(By.id("P0_SEARCH_VALUE")).sendKeys(msisdn);
		  
		   driver.findElement(By.id("P0_SEARCH")).submit();
		  
		   WebElement MSISDN= driver.findElement(By.id("P2_SI_MSISDN"));
		  
		   String MSIsdn= MSISDN.getText();
		  
		   Assert.assertEquals(MSIsdn, msisdn);
		  
		   driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
		  }
	   }
	
////////////////////////////////////////////////////////////////////////////////////////////////////
//******************************* Method to search by Account name ***************************
public void Search_name(String ACName)
{
  try {
	    Select dropdown = new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
		
	    dropdown.selectByVisibleText("Account Name");
		
	    driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
		
	    driver.findElement(By.id("P0_SEARCH_VALUE")).sendKeys(ACName);
		
	    driver.findElement(By.id("P0_SEARCH")).submit();
		
	    WebElement CustomerName= driver.findElement(By.id("BCD_NAME"));
		
	    Boolean CustomerNameTab= CustomerName.isDisplayed();
		
	    Assert.assertTrue(CustomerNameTab);
		
	    driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
		
  }catch(StaleElementReferenceException e)   {
	
	  Select dropdown = new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
		
	  dropdown.selectByVisibleText("Account Name");
		
	  driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
		
	  driver.findElement(By.id("P0_SEARCH_VALUE")).sendKeys(ACName);
		
	  driver.findElement(By.id("P0_SEARCH")).submit();
		
	  WebElement CustomerName= driver.findElement(By.id("BCD_NAME"));
		
	  Boolean CustomerNameTab= CustomerName.isDisplayed();
		
	  Assert.assertTrue(CustomerNameTab);
   }
  }
}
