package CRM_Tab;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Common_Methods.quickNoteTests;


public class QuickNoteTests extends quickNoteTests {

	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		SearchCustomerByAccountNumber(AccNo);
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void end()
	{
		driver.close();
	}

	@Test
	public void a_Click_QuickNotesFor_Subscriber() throws InterruptedException
	{
		clickQuickNotes();	
	}
	
	@Test
	public void b_Click_Create_Quicknote() throws InterruptedException
	{
		
		ClickCreateQuickNote();
		Thread.sleep(1000);
	}
	
	@Test
	public void c_Select_type() throws InterruptedException
	{
		Thread.sleep(1000);
		Select_Type();
	}
	
	@Test
	public void d_Enter_Subject()
	{
		Enter_subject();
	}
	
	@Test
	public void e_Enter_Note()
	{
		
		Enter_note();
	}
	
	@Test
	public void f_Select_Status()
	{
		Select_status();
	}
	
	@Test
	public void g_Select_FaultType()
	{
		Select_faultType();
	}
	
	@Test
	public void h_Click_Create()
	{
		Click_create();
	}
	
	@Test
	public void i_check_success_message()
	{
		suceess_msg();
	}
	
	@Test
	public void la_VerifyCreatedNote() throws InterruptedException 
	{
		Thread.sleep(1000);
		Verify_note();
		
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to click quick notes **********************************
public void clickQuickNotes() throws InterruptedException
{
	try {
	
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);
		
		WebElement QuickNotes= driver.findElement(By.id("B13528763358177637"));
		
		Boolean name= QuickNotes.isDisplayed();
		
		if(name==true)  {
		    
			wait.equals(name);
		    
			Actions act= new Actions(driver);
		    
			Thread.sleep(1500);
			
			act.moveToElement(QuickNotes).click().build().perform();
			
			Thread.sleep(1500);
		}
		
	}catch(StaleElementReferenceException e)    {
	
		WebElement QuickNotes= driver.findElement(By.id("B13528763358177637"));
		
		Actions act= new Actions(driver);
		
		Thread.sleep(1500);
		
		act.moveToElement(QuickNotes).click().build().perform();
		
		Thread.sleep(1500);
	}
  }
}
