package CRM_Tab;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;


public class Balance_enquiry_tests extends Common_Methods.CommonMethods {
		
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		Thread.sleep(1000);
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
		driver.quit();
	}
	
	@Test
	public void a_View_BalanceEnquiryTab() throws InterruptedException
	{
		View_BalanceEnquiry();
	}
	
	@Test
	public void b_Click_BalanceEnquiryTab()
	{
		Click_BalanceEnquiry();
	}
	
	@Test
	public void c_EnterMSISDN() throws InterruptedException
	{
		EnterMsisdn( MSISDN);
		Thread.sleep(1000);
	}
	
	@Test
	public void d_Enquire_Balance() throws InterruptedException
	{
		Thread.sleep(1500);
		enquire_Balance();
	}
	
	@Test
	public void e_VerifyEnquiry()
	{
		verifyEnquiry();
	}
///////////////////////////////////////////////////////////////////////////////////////////////////
//********************** Method to view balance enquiry tab ***********************************
public void View_BalanceEnquiry()
{
  try {
	
	  List<WebElement> anchors = driver.findElements(By.tagName("a"));
	  
	  java.util.Iterator<WebElement> i = anchors.iterator();
	  
	  while(i.hasNext()) {
	  
		  WebElement anchor = i.next();                 
	    
		  if(anchor.getText().contains("Balance Enquiry")) {
	    
			  System.out.println(anchor.getText());
	    
			  WebElement balanceEnquiry= driver.findElement(By.linkText("Balance Enquiry"));
	   
			  Boolean BEnquiry= balanceEnquiry.isDisplayed();
	   
			  Assert.assertTrue(BEnquiry);
	    
			  break;
	     
		  }else {
	   	 
			  System.out.println("Can not find Balance Enquiry tab");
	    }
	 }
	 
  }catch(StaleElementReferenceException |ElementNotFoundException e){
	
	  List<WebElement> anchors = driver.findElements(By.tagName("a"));
		
	  java.util.Iterator<WebElement> i = anchors.iterator();
		
	  while(i.hasNext()) {
		
		  WebElement anchor = i.next();                 
		  
		  if(anchor.getText().contains("Balance Enquiry"))  {
		        
			  System.out.println(anchor.getText());
		      
			  WebElement balanceEnquiry= driver.findElement(By.linkText("Balance Enquiry"));
		 	  
			  Boolean BEnquiry= balanceEnquiry.isDisplayed();
		 	  
			  Assert.assertTrue(BEnquiry);
		      
			  break;
		      
		  } else {
		  
			  System.out.println("Can not find Balance Enquiry tab");
		  }
	  }
	}
}


//**************************** Method to click Balance Enquiry tab *****************************
 public void Click_BalanceEnquiry()
 {
   try {
	   List<WebElement> anchors = driver.findElements(By.tagName("a"));
	   
	   java.util.Iterator<WebElement> i = anchors.iterator();
		
	   while(i.hasNext()) {
		
		   WebElement anchor = i.next();                 
		   
		   if(anchor.getText().contains("Balance Enquiry"))    {
		    
			   System.out.println(anchor.getText());
		    
			   Actions act= new Actions(driver);
		    
			   act.moveToElement(anchor).doubleClick().build().perform();
		     
			   break;
		     
		   }else {
		   
			   System.out.println("Can not find Balance Enquiry tab");
		   }
	   }
		 
   }catch(StaleElementReferenceException |ElementNotFoundException e){
					
	   List<WebElement> anchors = driver.findElements(By.tagName("a"));
		
	   java.util.Iterator<WebElement> i = anchors.iterator();
		
	   while(i.hasNext()) {
		
		   WebElement anchor = i.next();                 
			
		   if(anchor.getText().contains("Balance Enquiry")) {
			        
			   System.out.println(anchor.getText());
			   
			   Actions act= new Actions(driver);
			   
			   act.moveToElement(anchor).doubleClick().build().perform();
			   
			   break;
			   
		   } else {
			    
			   System.out.println("Can not find Balance Enquiry tab");
           }
	    }
     }
 }
 
 //******************************** Method to enter MSISDN ***********************************
public void EnterMsisdn(String MSISDN)
{
	WebElement msisdn= driver.findElement(By.id("P76_MSISDN"));

	msisdn.clear();	
	
	msisdn.sendKeys(MSISDN);
 }

//********************************* Method to click enquire button **************************
 public void enquire_Balance()
 {
	 WebElement enquire= driver.findElement(By.id("P76_ENQUIRE"));
	 
	 Actions act= new  Actions(driver);
	 
	 act.moveToElement(enquire).click().build().perform();
 }
 
//******************************* Method to verify balance ***********************************
public void verifyEnquiry()
{
	WebElement balance= driver.findElement(By.className("echo-report-links"));
	 
	Boolean Balance_report= balance.isDisplayed();
	 
	Assert.assertTrue(Balance_report);
   }
 }


