package CRM_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class Contact_Details_tab  extends Common_Methods.CommonMethods{
	
	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		SearchCustomerByAccountNumber(AccNo);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	  Thread.sleep(1000);
		driver.quit();
		
	}
	
	@Test
	public void a_View_contact_details_tab() throws InterruptedException
	{
		View_contactDetails();
	}
	
	@Test
	public void b_Access_contact_details_tab() throws InterruptedException
	{
		Access_contactDetails();
	}
	
	@Test
	public void c_View_contact_details() throws InterruptedException
	{
		view_cDetails();
	}
	
	@Test
	public void d_View_CreateContact()
	{
		View_CreateContact();
	}
	
	@Test
	public void e_Click_CreateContact()
	{
		Click_CreateContact();
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************* Method to view contact details tab****************************
public void View_contactDetails()
{
	WebElement contactDetails= driver.findElement(By.xpath("//a[contains(text(),'Customer Contact Details')]"));
		
	Boolean details= contactDetails.isDisplayed();
		
	Assert.assertTrue(details);
	
}

//************************************* Method to access contact details tab****************************
public void Access_contactDetails() throws InterruptedException
{
	Thread.sleep(1500);
	
	WebElement contactDetails= driver.findElement(By.xpath("//a[contains(text(),'Customer Contact Details')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(contactDetails).click().build().perform();
			
}
	
//************************************* Method to view contact details of customer ********************
public void view_cDetails() throws InterruptedException
{
	Thread.sleep(1500);
			
	WebElement contactDetails= driver.findElement(By.xpath("//span[@id='P2_BCD_CONTACT_NAME']"));
			
	Boolean details= contactDetails.isDisplayed();
			
	Assert.assertTrue(details);
			
}

//************************************ Method to view create contact option ***********************
public void View_CreateContact()
{
	WebElement Create= driver.findElement(By.id("B35940000776364066"));
			
	Boolean contact= Create.isDisplayed();
			
	Assert.assertTrue(contact);
	
}
		
//*********************************** Method to click Create contact option ***********************
public void Click_CreateContact()
{
	WebElement Create= driver.findElement(By.id("B35940000776364066"));
			
	Actions act= new Actions(driver);
			
	act.moveToElement(Create).doubleClick().build().perform();
	
   }
}
