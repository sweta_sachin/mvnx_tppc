package Ticketing_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Reminders_Tab_tests  extends Common_Methods.CommonMethods{
	
	// PPD does not have data to validate against it,need data to cover all the scenarios 	

	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_TicketingTab();
		Thread.sleep(200);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
		
	 Thread.sleep(1000);
	 driver.quit();
	 
	}

	@Test
	public void a_View_RemindersTab() throws InterruptedException
	{
		Thread.sleep(500);
		view_RemindersTab();
	}

	@Test
	public void b_Click_RemindersTab() throws InterruptedException
	{
		Thread.sleep(500);
		click_Reminders();
	}
	
	@Test
	public void g_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void h__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void i_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(500); 
		search_by_private_report();
	}
	
	@Test
	public void j_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
		
	@Test
	public void k_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","reminders.csv"));
	}
	
	@Test
	public void l_Send_Subscription_Email() throws InterruptedException
	{
		Thread.sleep(300);
		get_subscription();
		
	}
		
	@Test
	public void m_reset_test() throws InterruptedException
	{
		Thread.sleep(500);
		reset();
	}
	

	@Test
	public void n_flashBackTest() throws InterruptedException
	{
		Thread.sleep(500);
		flashback_filter();
	}
	
	
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Reminders tab ******************************
public void view_RemindersTab()
 {
	WebElement AG= driver.findElement(By.linkText("Reminders"));
	
	Boolean ag= AG.isDisplayed();
	
	Assert.assertTrue(ag);
}


//************************************** Method to click Reminders tab ******************************
public void click_Reminders() throws InterruptedException
{
	WebElement Ag= driver.findElement(By.linkText("Reminders"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Ag).click().build().perform();
	
	Thread.sleep(500);
	
	// Verify report
	
	Boolean ag= driver.findElement(By.id("apexir_SEARCH")).isDisplayed();
	
	Assert.assertTrue(ag);
		
	}

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
 {
	 col= "Status";
	 expvalue= "Open";
	
	 view_filtered_report();
	 
	// Verify report
	
	 Thread.sleep(300);
	
	 String verifyreport= driver.findElement(By.xpath("//div[@id='apexir_WORKSHEET']//div//td[8]")).getText();
	
	 Thread.sleep(300);
	
	 Assert.assertEquals(verifyreport, expvalue);
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 
	
	save_report();

	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();
	
	// verify 
	
	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
}


//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
 {
	select_primary_report();

	Thread.sleep(300);
	
	try {

		WebElement primaryreport= driver.findElement(By.id("1201507579502749"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	
	}catch(Exception e) 	{

		WebElement primaryreport= driver.findElement(By.id("1201507579502749"));
	
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}

}
//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
	{
		Reset();
		
	// Verify report
		
	WebElement primaryreport= driver.findElement(By.id("1201507579502749"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
}	

}
