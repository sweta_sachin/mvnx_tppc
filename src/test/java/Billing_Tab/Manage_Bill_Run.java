package Billing_Tab;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Common_Methods.CommonMethods;

public class Manage_Bill_Run extends CommonMethods {
	
	
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		Thread.sleep(1000);
		click_billingTab();
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
		driver.quit();
	}
	
  @Test
  public void a_ViewManage_BillRun_Tab()
  {
	  view_ManageBillRunTab();
  }
  
  @Test
  public void b_ClickManage_BillRun_Tab() throws InterruptedException
  {
	 Thread.sleep(1000);
	  click_ManageBillRunTab();
  }
  
  @Test
  public void c_ViewAllTestBillRuns() throws InterruptedException
  {
	  view_allTestBillRuns();
  }
  
  @Test
  public void d_ViewAllFullBillRuns() throws InterruptedException
  {
	  view_allFullBillRuns();
  }
  
  @Test
  public void e_ViewAutoTestRefreshBillRuns() throws InterruptedException
  {
	  view_autoTestRefreshBillRuns();
  }
  
  
  @Test
  public void f_ViewInstantBill_AccountBillRun() throws InterruptedException
  {
	  view_instantBillAccount_Billrun();
  }
  

  @Test
  public void g_ViewInstantBill_SubscriberBillRun() throws InterruptedException
  {
	  view_instantBillSubscriber_Billrun();
  }
  
  @Test
  public void h_CreateTestBillRun() throws InterruptedException, ParseException
  {
	  Create_Test_Billrun();
  }
  
  @Test
  public void x_CreateFullBillRun() throws InterruptedException, ParseException
  {
	  Create_Full_Billrun();
  }
  
  @Test
  public void j_CreateAutoTestRefreshBillRun() throws InterruptedException, ParseException
  {
	  Create_AutoTestRefresh_Billrun();
  }
  

  @Test
  public void z_InstantBillRun_Account() throws InterruptedException, ParseException
  {
	  Create_InstantBillRun_Account();
  }
  
  @Test
  public void y_InstantBillRun_Subscriber() throws InterruptedException, ParseException
  {
	  Create_InstantBillRun_Subscriber();
  }
  
  
///////////////////////////////////////////////////////////////////////////////////////////////////
//**************************** Method to click Billing tab *****************************
public void click_billingTab()
 {
	WebElement billing=  driver.findElement(By.linkText("Billing"));

	Actions act= new Actions(driver);
	
	act.moveToElement(billing).click().build().perform();
  }
  
  
//**************************** Method to view Manage Bill run tab *****************************
public void view_ManageBillRunTab()
{
	WebElement Manage=  driver.findElement(By.linkText("Manage Bill Runs"));

	Boolean ManageBillrun_tab= Manage.isDisplayed();

	Assert.assertTrue(ManageBillrun_tab);
}

//**************************** Method to click manage bill run tab *****************************
public void click_ManageBillRunTab()
{
	WebElement Manage=  driver.findElement(By.linkText("Manage Bill Runs"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Manage).click().build().perform();
}

//**************************** Method to view All Test Bill runs *****************************
public void view_allTestBillRuns() throws InterruptedException
{
  
	Select dropdown= new Select(driver.findElement(By.id("P15_BBR_BBRT_UID")));
  
	dropdown.selectByVisibleText("Test Bill Run");
  
	Thread.sleep(1200);

	WebElement type=  driver.findElement(By.xpath("//tr[2]//td[3]"));

	String Actual= type.getText();

	String Expected= "Test Bill Run";

	Assert.assertEquals(Actual, Expected);
}

//**************************** Method to view All Full Bill runs *****************************
public void view_allFullBillRuns() throws InterruptedException
{

	Select dropdown= new Select(driver.findElement(By.id("P15_BBR_BBRT_UID")));

	dropdown.selectByVisibleText("Full Bill Run");

	Thread.sleep(1200);

	WebElement type=  driver.findElement(By.xpath("//tr[2]//td[3]"));

	String Actual= type.getText();

	String Expected= "Full Bill Run";

	Assert.assertEquals(Actual, Expected);
}

//**************************** Method to view Auto test refresh Bill runs *****************************
public void view_autoTestRefreshBillRuns() throws InterruptedException
{

	Select dropdown= new Select(driver.findElement(By.id("P15_BBR_BBRT_UID")));

	dropdown.selectByVisibleText("Auto Test Refresh Bill Run");

	Thread.sleep(1200);

	WebElement type=  driver.findElement(By.xpath("//tr[2]//td[3]"));

	String Actual= type.getText();

	String Expected= "Auto Test Refresh Bill Run";

	Assert.assertEquals(Actual, Expected);
}

//**************************** Method to view Instant Bill- Account Bill runs *****************************
public void  view_instantBillAccount_Billrun() throws InterruptedException
{

	Select dropdown= new Select(driver.findElement(By.id("P15_BBR_BBRT_UID")));

	dropdown.selectByVisibleText("Instant Bill - Account");

	Thread.sleep(1200);

	WebElement type=  driver.findElement(By.xpath("//tr[2]//td[3]"));

	String Actual= type.getText();

	String Expected= "Instant Bill - Account";

	Assert.assertEquals(Actual, Expected);
}


//**************************** Method to view Instant Bill- subscriber Bill runs *****************************
public void  view_instantBillSubscriber_Billrun() throws InterruptedException
{

	Select dropdown= new Select(driver.findElement(By.id("P15_BBR_BBRT_UID")));

	dropdown.selectByVisibleText("Instant Bill - Subscriber");

	Thread.sleep(1200);

	WebElement type=  driver.findElement(By.xpath("//tr[2]//td[3]"));

	String Actual= type.getText();

	String Expected= "Instant Bill - Subscriber";

	Assert.assertEquals(Actual, Expected);
}

//**************************** Method to click create button *****************************
public void click_createButton()
{
	WebElement create=  driver.findElement(By.linkText("Create"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(create).click().build().perform();
}

//**************************** Method to create Test Bill run *****************************
public void Create_Test_Billrun() throws InterruptedException, ParseException
{
	click_createButton();
	
	Select dropdown= new Select(driver.findElement(By.id("P18_BILL_RUN_TYPE")));
	
	dropdown.selectByVisibleText("Test Bill Run");
	
	Thread.sleep(800);
	
	//Select Bill Cycle 
	
	Select billcycle= new Select(driver.findElement(By.id("P18_BBR_BBC_UID")));
	
	billcycle.selectByIndex(2);
	
	Thread.sleep(800);
	
	click_createButton();
	
	Thread.sleep(800);
	
	Select dropdown1= new Select(driver.findElement(By.id("P15_BBR_BBRT_UID")));
	
	dropdown1.selectByVisibleText("Test Bill Run");
	
	Thread.sleep(800);
		
	WebElement datestamp=  driver.findElement(By.xpath("//tr[2]//td[15]"));
	
	String Date= datestamp.getText();
		
	System.out.println(Date);
	
	DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
	
	Date date = new Date();
	
	String date1= dateFormat.format(date);
	
	Date Date2 = dateFormat.parse(Date);
	
	String date2= dateFormat.format(Date2);
	
	System.out.println(date1);
	
	System.out.println(date2);
	
	Assert.assertEquals(date2, date1);
	
}


//**************************** Method to create Full Bill run *****************************
public void Create_Full_Billrun() throws InterruptedException, ParseException
{
	Thread.sleep(800);

	click_createButton();
	
	Select dropdown= new Select(driver.findElement(By.id("P18_BILL_RUN_TYPE")));
	
	dropdown.selectByVisibleText("Full Bill Run");
	
	Thread.sleep(800);
	
	//Select Bill Cycle 
	
	Select billcycle= new Select(driver.findElement(By.id("P18_BBR_BBC_UID")));
	
	billcycle.selectByIndex(2);
	
	Thread.sleep(800);
	  
	//Select Workflow
	
	Select workflow= new Select(driver.findElement(By.id("P18_WF_CODE")));
	
	workflow.selectByVisibleText("Create Bill Run Workflow");;
	
	click_createButton();
	
	Thread.sleep(800);
		
	//Click Home
	
	WebElement home= driver.findElement(By.linkText("Home"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(home).click().build().perform();
	
	Thread.sleep(500);
	  
	//Click manage bill run tab
	
	click_ManageBillRunTab();
	
	Thread.sleep(300);
		
	Select dropdown1= new Select(driver.findElement(By.id("P15_BBR_BBRT_UID")));
	
	dropdown1.selectByVisibleText("Full Bill Run");
	
	Thread.sleep(800);
	
	WebElement datestamp=  driver.findElement(By.xpath("//tr[2]//td[15]"));
	
	String Date= datestamp.getText();
	
	System.out.println(Date);
	
	DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
	
	Date date = new Date();
	
	String date1= dateFormat.format(date);
	
	Date Date2 = dateFormat.parse(Date);
	
	String date2= dateFormat.format(Date2);
	
	System.out.println(date1);
	
	System.out.println(date2);
	
	Assert.assertEquals(date2, date1);
	
}

//**************************** Method to create Auto Test Refresh Bill run *****************************
public void Create_AutoTestRefresh_Billrun() throws InterruptedException, ParseException
{
	click_createButton();
	
	Select dropdown= new Select(driver.findElement(By.id("P18_BILL_RUN_TYPE")));
	
	dropdown.selectByVisibleText("Auto Test Refresh Bill Run");
	
	Thread.sleep(800);
	
	//Select Bill Cycle 
	
	Select billcycle= new Select(driver.findElement(By.id("P18_BBR_BBC_UID")));
	
	billcycle.selectByIndex(2);
	
	Thread.sleep(800);
	
	click_createButton();
	
	Thread.sleep(800);
	
	Select dropdown1= new Select(driver.findElement(By.id("P15_BBR_BBRT_UID")));
	
	dropdown1.selectByVisibleText("Auto Test Refresh Bill Run");
	
	Thread.sleep(800);
	
	WebElement datestamp=  driver.findElement(By.xpath("//tr[2]//td[15]"));
	
	String Date= datestamp.getText();
	
	System.out.println(Date);
	
	DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
	
	Date date = new Date();
	
	String date1= dateFormat.format(date);
	
	Date Date2 = dateFormat.parse(Date);
	
	String date2= dateFormat.format(Date2);
		
	System.out.println(date1);

	System.out.println(date2);
	
	Assert.assertEquals(date2, date1);
	
}

//**************************** Method to create Instant Bill - Account *****************************
public void Create_InstantBillRun_Account() throws InterruptedException, ParseException
{
	click_createButton();

	Select dropdown= new Select(driver.findElement(By.id("P18_BILL_RUN_TYPE")));
	
	dropdown.selectByVisibleText("Instant Bill - Account");
	
	Thread.sleep(800);
	
	//Select Bill Cycle 
	
	Select billcycle= new Select(driver.findElement(By.id("P18_BBR_BBC_UID")));
	  
	billcycle.selectByIndex(2);
	
	Thread.sleep(800);
	
	click_createButton();
	
	Thread.sleep(800);
	
	Select dropdown1= new Select(driver.findElement(By.id("P15_BBR_BBRT_UID")));
	
	dropdown1.selectByVisibleText("Instant Bill - Account");
	
	Thread.sleep(800);
	
	WebElement datestamp=  driver.findElement(By.xpath("//tr[2]//td[15]"));
	
	String Date= datestamp.getText();
	
	System.out.println(Date);
	
	DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
	
	Date date = new Date();
	
	String date1= dateFormat.format(date);
	
	Date Date2 = dateFormat.parse(Date);
	
	String date2= dateFormat.format(Date2);
	
	System.out.println(date1);
	
	System.out.println(date2);
	
	Assert.assertEquals(date2, date1);
	
}


//**************************** Method to create Instant Bill - Subscriber *****************************
public void Create_InstantBillRun_Subscriber() throws InterruptedException, ParseException
{
	click_createButton();

	Select dropdown= new Select(driver.findElement(By.id("P18_BILL_RUN_TYPE")));
	
	dropdown.selectByVisibleText("Instant Bill - Subscriber");
	
	Thread.sleep(800);
		 
	//Click search
	
	WebElement search= driver.findElement(By.id("P18_SEARCH"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(search).click().build().perform();
	
	Thread.sleep(800);
	  
	//Select the subscriber to be added
	
	WebElement subscriber= driver.findElement(By.xpath("//div[@id='apexir_WORKSHEET_REGION']//tr[2]//td[1]//input[1]"));
	
	subscriber.click();
		 
	//Click "Show selected subscriber button"
	  
	WebElement show_subscriber= driver.findElement(By.id("B198686574560852010"));
	
	act.moveToElement(show_subscriber).click().build().perform();
	
	Thread.sleep(800);
	
	click_createButton();
	
	Select dropdown1= new Select(driver.findElement(By.id("P15_BBR_BBRT_UID")));
	
	dropdown1.selectByVisibleText("Instant Bill - Subscriber");
	
	WebElement datestamp=  driver.findElement(By.xpath("//tr[2]//td[15]"));
		
	String Date= datestamp.getText();
	
	System.out.println(Date);
	
	DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
	
	Date date = new Date();
	
	String date1= dateFormat.format(date);
	
	Date Date2 = dateFormat.parse(Date);
	
	String date2= dateFormat.format(Date2);
	
	System.out.println(date1);
		
	System.out.println(date2);
	
	Assert.assertEquals(date2, date1);
	}
}
