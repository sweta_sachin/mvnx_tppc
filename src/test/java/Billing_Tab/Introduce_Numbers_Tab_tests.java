package Billing_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Introduce_Numbers_Tab_tests  extends Common_Methods.CommonMethods{
	
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		Thread.sleep(1000);
		click_billingTab();
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
		driver.quit();
	}
	
  @Test
  public void a_View_IntroduceNumbers_Tab()
  {
	  view_IntroduceNumbersTab();
  }
  
  @Test
  public void b_Click_IntroduceNumbers_Tab() throws InterruptedException
  {
	 Thread.sleep(1000);
	  click_IntroduceNumbersTab();
  }
  
   @Test
 	public void c_Filter_report() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		View_filtered_report();
 	}
 	
 	@Test
 	public void d_Save_Report() throws InterruptedException
 	{
 		Thread.sleep(1500); 
 		Save_report();
 	}
 	
 	@Test
 	public void e_Search_by_privateReport() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		search_by_private_report();
 	}
 	
 	@Test
 	public void f_search_By_Primary_report() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		search_by_PrimaryReports();
 	}
 	
 	@Test
 	public void g_flashBackTest() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		flashback_filter();
 	}
 	
 	@Test
 	public void h_reset_test() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		reset();
 	}
 	
 	@Test
 	public void i_DownloadCSV() throws InterruptedException
 	{
 		Thread.sleep(1500);
 		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","introduce_numbers.csv"));
 	}
 	
	@Test
	public void j_Subscdription_email() throws InterruptedException
	{
		Thread.sleep(300);
		get_subscription();
	}
	
	@Test
	public void k_Create_Category() throws InterruptedException
	{
		Thread.sleep(300);
		IntroduceNumbers();
		
	}
	
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//**************************** Method to view Introduce Numbers tab *****************************
public void view_IntroduceNumbersTab()
{

	WebElement IntroduceNumbers=  driver.findElement(By.linkText("Introduce Numbers"));
	
	Boolean IntroduceNumbers_tab= IntroduceNumbers.isDisplayed();
	
	Assert.assertTrue(IntroduceNumbers_tab);
	
}

//**************************** Method to click Introduce Numbers tab *****************************
public void click_IntroduceNumbersTab() throws InterruptedException
{

	WebElement IntroduceNumbers=  driver.findElement(By.linkText("Introduce Numbers"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(IntroduceNumbers).click().build().perform();
	
	Thread.sleep(400);
		
	//Verify
	
	Boolean verify= driver.findElement(By.id("apexir_SEARCH")).isDisplayed();
	
	Assert.assertTrue(verify);
	
}
	
//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	expvalue= "Pending";
	col= "Status";

	view_filtered_report();
		
	//Verify report
	
	Thread.sleep(400);
	
	String verifyreport= driver.findElement(By.xpath("//tr[2]//td[8]")).getText();
	
	Thread.sleep(300);
	
	Assert.assertEquals(verifyreport, expvalue);
	
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 

	save_report();
		
	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
		 	 
	}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();	
			
	// verify 

	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
			
}

//**************************************** Method to search primary report ****************************
public void search_by_PrimaryReports() throws InterruptedException
{
	try {
	
		select_primary_report();
		
		Thread.sleep(500);
		
		WebElement primaryreport= driver.findElement(By.id("158655854392837484"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
	} catch(Exception e){
	
		select_primary_report();
		
		Thread.sleep(300);
		
		WebElement primaryreport= driver.findElement(By.id("158655854392837484"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
	}
		
	}

//************************************ Method to test reset button on CIB ******************************
public void reset() throws InterruptedException
{
	Thread.sleep(200);

	Reset();
		 
	// Verify report
	
	WebElement primaryreport= driver.findElement(By.id("158655854392837484"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
	
}

//************************************** Method to introduce numbers ***********************************
public void IntroduceNumbers() throws InterruptedException
{
	//Click Create

	WebElement CLICK= driver.findElement(By.id("B158656859828837496"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(CLICK).click().build().perform();
	
	Thread.sleep(300);
	
	//Select category
	
	Select category= new Select(driver.findElement(By.id("P33_NA_UID")));
	
	category.selectByIndex(2);
	
	//Click go
	
	WebElement go= driver.findElement(By.id("P33_GO"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(go).click().build().perform();
	
	Thread.sleep(4000);
	
	//Check success message
	
	String Msg= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[2]/div[1]")).getText();
	
	String Expected_msg= "Numbers introduced successfully";
	
	Assert.assertEquals(Msg, Expected_msg);
		
	//Enter Quatity 
	
	WebElement quantity= driver.findElement(By.id("P33_QUANTITY"));
	
	quantity.clear();
	
	quantity.sendKeys("5");
		
	//Click Create
	
	WebElement create= driver.findElement(By.id("B158697446792442204"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(create).click().build().perform();
	
	Thread.sleep(300);
		
	//Check success message
	
	String Mssg= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[2]/div[1]")).getText();
	
	String Expected_mssg= "Job submitted successfully";
	
	Assert.assertEquals(Mssg, Expected_mssg);
			
  }

}
