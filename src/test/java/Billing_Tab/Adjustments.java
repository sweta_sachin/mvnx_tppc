package Billing_Tab;

import java.io.File;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Adjustments extends Common_Methods.CommonMethods {
	
	
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		Thread.sleep(1000);
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
		driver.quit();
	}
	
  @Test
  public void a_View_Billing_Tab()
  {
	  view_billingTab();
  }
  
  @Test
  public void b_Click_Billing_Tab()
  {
	  click_billingTab();
  }
  
	
  @Test
  public void c_View_Adjustment_Tab()
  {
	  view_adjustmentTab();
  }
  
  @Test
  public void d_Click_Adjustment_Tab() throws InterruptedException
  {
	 Thread.sleep(1000);
	  click_adjustmentTab();
  }
  
  @Test
  public void e_View_CreateButton()
  {
	  view_createButton();
  }
  
  @Test
  public void f_Click_CreateButton() throws InterruptedException
  {
	 Thread.sleep(1000);
	  click_createButton();
  }
  
  @Test
  public void g_AddDescription() throws InterruptedException
  {
	 Thread.sleep(1000);
	  Add_description();
  }
  
  @Test
  public void h_AddAttachment() throws InterruptedException
  {
	 Thread.sleep(1000);
	 Attach_approvedDoc();
  }
  
  @Test
  public void i_ClickCreate() throws InterruptedException
  {
	 Thread.sleep(1000);
	 click_createButton();
  }
  
  @Test
  public void j_checkSuccessMsg() throws InterruptedException
  {
	  Thread.sleep(800);
		Check_SuccessMsg();
  }
  
  @Test
  public void k_verifyAdjustment() throws InterruptedException
  {
	  Thread.sleep(800);
	  Verify_Adjustment();
  }
  
  @Test
  public void l_verifyDownloading() throws InterruptedException
  {
	  Thread.sleep(800);
	 Assert.assertTrue( Verify_Downloading("C:\\Users\\Sweta\\Downloads","test doc.pdf"));
	  
  }
  
   @Test
  public void m_editAdjustment() throws InterruptedException
  {
	  Thread.sleep(800);
	 Edit_Adjustment();
  }
  
   @Test
   public void n_deleteAdjustment() throws InterruptedException
   {
 	  Thread.sleep(800);
 	 Delete_Adjustment();
   }
   
  
  
///////////////////////////////////////////////////////////////////////////////////////////////////
//**************************** Method to view Billing tab *****************************
public void view_billingTab()
 {
	WebElement billing=  driver.findElement(By.linkText("Billing"));

	Boolean Billing_tab= billing.isDisplayed();
	
	Assert.assertTrue(Billing_tab);
  }
  
//**************************** Method to click Billing tab *****************************
public void click_billingTab()
 {
	WebElement billing=  driver.findElement(By.linkText("Billing"));

	Actions act= new Actions(driver);
	
	act.moveToElement(billing).click().build().perform();
  }
  
//**************************** Method to view adjustment tab *****************************
public void view_adjustmentTab()
 {
	WebElement adjustment=  driver.findElement(By.linkText("Adjustments"));

	Boolean adjustment_tab= adjustment.isDisplayed();
	
	Assert.assertTrue(adjustment_tab);
  }
  
//**************************** Method to click adjustment tab *****************************
public void click_adjustmentTab()
 {
	  WebElement adjustment=  driver.findElement(By.linkText("Adjustments"));

	  Actions act= new Actions(driver);
	
	  act.moveToElement(adjustment).click().build().perform();
  }
  
//**************************** Method to view create button *****************************
public void view_createButton()
 {
	WebElement create=  driver.findElement(By.linkText("Create"));

	Boolean create_button= create.isDisplayed();
	
	Assert.assertTrue(create_button);
  }
  
//**************************** Method to click create button *****************************
public void click_createButton()
 {
	  WebElement create=  driver.findElement(By.linkText("Create"));

	  Actions act= new Actions(driver);
	  
	  act.moveToElement(create).click().build().perform();
  }
  
//**************************** Method to add description to the adjustment batch *****************************
public void Add_description()
 {
 
	WebElement description=  driver.findElement(By.id("P13_BBRA_DESCRIPTION"));

	description.sendKeys("Test");
  
 }
  
//**************************** Method to attach pdf to the adjustment batch *****************************
public void Attach_approvedDoc()
 {
	  WebElement doc=  driver.findElement(By.id("P13_BBRA_DOCUMENT"));

	  doc.sendKeys("C:\\Users\\Sweta\\Desktop\\test doc.pdf");
  }

//***************************** Method to check success message after adjustment gets created ***********
public void Check_SuccessMsg()
{
	String Msg= driver.findElement(By.id("echo-message")).getText();
		
	String Expected_msg= "Action Processed.";
		
	Assert.assertEquals(Msg, Expected_msg);
  }

//************************** Method to verify if adjustment is added to the list **********
public void  Verify_Adjustment()
 {
	WebElement datestamp=  driver.findElement(By.xpath("//tr[2]//td[12]"));

	String Date= datestamp.getText();
	
	System.out.println(Date);
	
	DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
	
	Date date = new Date();
	
	String date1= dateFormat.format(date);
	
	System.out.println(date1);
	
	Assert.assertEquals(Date, date1);
  
 }
  
//****************************************** Method to edit adjustment *****************************
public void Edit_Adjustment() throws InterruptedException
{  
	WebElement edit=  driver.findElement(By.xpath("//tr[2]//td[1]//a[1]//img[1]"));

	Actions act= new Actions(driver);
	
	act.moveToElement(edit).click().build().perform();
	
	Thread.sleep(500);
	
	WebElement description=  driver.findElement(By.id("P13_BBRA_DESCRIPTION"));
	
	description.sendKeys("Editing Done");
	
	WebElement Apply_Changes=  driver.findElement(By.id("B166660165729860455"));
	
	act.moveToElement(Apply_Changes).click().build().perform();	
	
	Thread.sleep(500);
	
	Check_SuccessMsg();
		
  }
  
//****************************************** Method to delete adjustment *****************************
public void Delete_Adjustment() throws InterruptedException
{  
	WebElement ID=  driver.findElement(By.xpath("//tr[2]//td[2]")); 
	
	String id= ID.getText();
	
	System.out.print("ID before delete" +id);
	
	WebElement edit=  driver.findElement(By.xpath("//tr[2]//td[1]//a[1]//img[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(edit).click().build().perform();
	
	Thread.sleep(500); 
	
	WebElement delete=  driver.findElement(By.id("B166660251066860455"));
	
	act.moveToElement(delete).click().build().perform();
	
	try {
	
		Alert alert= driver.switchTo().alert();
		
		alert.accept();
		
	}catch(UnhandledAlertException | NoAlertPresentException e)  {
	
		Alert alert= driver.switchTo().alert();
		
		alert.accept();
		
	}
	
	Check_SuccessMsg();
	
	WebElement ID1=  driver.findElement(By.xpath("//tr[2]//td[2]")); 
	
	String id1= ID1.getText();
		
	System.out.print("ID after " +id1);
	
	Assert.assertNotEquals(id, id1);
    
}
  
//****************************************** Method to verify adjustment *****************************
public boolean  Verify_Downloading(String downloadPath, String fileName) throws InterruptedException
{  
	WebElement downloading= driver.findElement(By.xpath("//tr[2]//td[2]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(downloading).click().build().perform();
	
	Thread.sleep(1000);
	
	File dir = new File(downloadPath);
	
	File[] dirContents = dir.listFiles();
	
	for (int i = 0; i < dirContents.length; i++) {
		
		if (dirContents[i].getName().equals(fileName)) {
		
			// File has been found, it can now be deleted:
		    
			dirContents[i].delete();
		    
			System.out.println("File is getting deleted");
		    
			return true;
		}
	}
	
	return false;
  }
}
