package Common_Methods;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;

public class CommonMethods {
	
	public static WebDriver driver;
	
	public static String BaseUrl = "http://vxview.ppd.mvnxmobile.co.za/ords/f?p=UA:101";
	
	public static String username = "smasleka";
	
	public static String password ="test@123";
	
	public static String AccNo= "PPC0000016";
		
	public static String UID= "259439399"; // uid number to search test on CRM
	
	public static String ICCID= "1234"; //icicd used for sim swap tests
	
	public static String msisdn= "27626584276"; // active  msisdn number for search and other multiple tests
	
	public static String AccName= "Blue Label Connect"; // Account name should be valid and available to search on credit control tab
	
	public static String CNumber= "27111234567"; // contact number should be valid and available to search on credit control tab
	
	public static String AcType= "Individual"; //used in multiple tests no need to change
	
	public static String TaxRate= "Standard VAT 15%";
	
	public static String Newname= "new name";  // this is commonly used string and no need to change every time
	
	public static int Index= 1; // should be unique
	
	public static String Firstname= "gast"; // should be always unique and used to create user
	
	public static String surname= "verify"; // should be always unique and used to create user
		
	public static String logon= "gverify"; //this should be combination of first name and surname
	
	public static String Password= "gve@123"; // should be 1st 3 letters of log on name should be of 7 characters
		
	public static String MSISDN= "27650401697"; // should be always unique and used for MSISDN swap
		
	public static String subject= "test subject"; // this is commonly used string and no need to change every time
	
	public static String Editsubject= "Edited test subject"; // this is commonly used string and no need to change every time
	
	public static String Comment= "test comment"; // this is commonly used string and no need to change every time
		
	public static String Name= "q12"; // should be unique used in multiple tests to create 
	
	public static String Name1= "r12"; // should be unique used in multiple tests to edit 
	
	public static String toemail = "email@test.com"; // dummy email id for user create test and for send email report or subscription email
	
	public static String Time = "10"; // time for flash back test
	
	public static String flashback_msg= "10 minutes ago.   ";// number in this should be same as flash back time (now it is 10)
	
	public  String expvalue ; // standard test parameters initialized for individual tests
	
	public  String report ;	// standard test parameters initialized for individual tests
	
	public  String col;	// standard test parameters initialized for individual tests
	
	public static String reportname= "Test report"; 	// standard test parameters initialized for individual tests
	
	public static String verifyreport= "Saved Report = \"Test report\"";	// standard test parameters initialized for individual tests
	
	public static  String msisdn1= "25845678456"; // MSISDN for introduce numbers tests this should be starting of range
	
	public static String msisdn2= "25888888888"; // MSISDN for introduce numbers tests this should be end of range
	
	public static String DATE ="01-Jan-2017"; //date should be used in multiple tests while creating 

	
//****************************** Method to login to MVNX PROD*****************************************
public void Login()
{
	System.setProperty("webdriver.chrome.driver", "E:\\ChromeDriver\\chromedriver.exe");
	
	DesiredCapabilities caps = DesiredCapabilities.chrome();
	
	LoggingPreferences logPrefs = new LoggingPreferences();
	
	logPrefs.enable(LogType.BROWSER, Level.ALL);
	
	caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
	
	driver = new ChromeDriver(caps);
	
	driver.get(BaseUrl);	
	
	driver.manage().window().maximize();
	
	driver.findElement(By.id("P101_USERNAME")).sendKeys(username);
	
	driver.findElement(By.id("P101_PASSWORD")).sendKeys(password);
	
	Select dropdown = new Select(driver.findElement(By.id("P101_VN_UID")));
	
	dropdown.selectByVisibleText("The Post Paid Company");
	
	driver.findElement(By.id("P101_LOGIN")).submit();
	
}
	
//************************************** Method to click on CRM tab**************************************	
public void clickCRMTab() 
{		
	try {
	
		List<WebElement> anchors = driver.findElements(By.tagName("a"));
    
		java.util.Iterator<WebElement> i = anchors.iterator();
    
		while(i.hasNext()) {
    
			WebElement anchor = i.next();                 
    
			if(anchor.getText().contains("CRM"))   {
    
				System.out.println(anchor.getText());
    
				Actions act= new Actions(driver);
    
				act.moveToElement(anchor).doubleClick().build().perform();
     
				break;
     
			}else {
   	 
				System.out.println("Can not find CRM tab");
			}
		}
 }catch(StaleElementReferenceException |ElementNotFoundException e){
		
	 List<WebElement> anchors = driver.findElements(By.tagName("a"));
	
	 java.util.Iterator<WebElement> i = anchors.iterator();
	 
	 while(i.hasNext()) {
	 
		 WebElement anchor = i.next();                 
	     
		 if(anchor.getText().contains("CRM"))  {
	       
			 System.out.println(anchor.getText());
	        
			 Actions act= new Actions(driver);
	        
			 act.moveToElement(anchor).doubleClick().build().perform();
	         
			 break;
	        
		 } else {
	     
			 System.out.println("Can not find CRM tab");
	        }
	     }
	  }
	}
	
//************************************* Method to click Credit control tab********************************
public void clickCreditControlTab() 
{   
	try {
	
		List<WebElement> anchors = driver.findElements(By.tagName("a"));
		
		java.util.Iterator<WebElement> i = anchors.iterator();
        
		while(i.hasNext()) {
        
			WebElement anchor = i.next(); 
        
			if(anchor.getText().contains("Credit Control"))  {
        
				System.out.println(anchor.getText());
        
				Actions act= new Actions(driver);
        
				act.moveToElement(anchor).doubleClick().build().perform();
          
				break;
          
			} else {
       	 
				System.out.println("Can not find credit control tab");
			}
		}
	}catch (StaleElementReferenceException  e) {
		
		List<WebElement> anchors = driver.findElements(By.tagName("a"));
		
		java.util.Iterator<WebElement> i = anchors.iterator();
	    
		while(i.hasNext()) {
	    
			WebElement anchor = i.next(); 
	        
			if(anchor.getText().contains("Credit Control"))   {
	        
				System.out.println(anchor.getText());
	        
				Actions act= new Actions(driver);
	         
				act.moveToElement(anchor).doubleClick().build().perform();
	         
				break;
	       
			} else {
	       	
				System.out.println("Can not find credit control tab");
	        }
	      }
		}
    }

//***************************************Method to search customer by account number*******************
public void SearchCustomerByAccountNumber(String AccNo)
{
     Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
    		 .withTimeout(30, TimeUnit.SECONDS) 			
    		 .pollingEvery(1, TimeUnit.SECONDS) 			
    		 .ignoring(NoSuchElementException.class);
			 
     WebElement searchtype= driver.findElement(By.id("P0_SEARCH_TYPE"));
	
     Boolean his= searchtype.isDisplayed();
	
     wait.equals(his);
	
     Select dropdown = new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
	
     dropdown.selectByVisibleText("Account Number");
	
     driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
	
     driver.findElement(By.id("P0_SEARCH_VALUE")).sendKeys(AccNo);
	
     try {
	
    	 driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		 
    	 driver.findElement(By.id("P0_SEARCH")).submit();
		 
     }catch(NoSuchElementException| StaleElementReferenceException e){
		 			
    	 driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		 
    	 driver.findElement(By.id("P0_SEARCH")).submit();
     }
}


//***************************************************Method to edit customer*************************
public void ClickEdit_Customer()
{
	try {
	
		List<WebElement> anchors = driver.findElements(By.tagName("a"));
		
		java.util.Iterator<WebElement> i = anchors.iterator();
	    
		while(i.hasNext()) {
	    
			WebElement anchor = i.next();                 
	    
			if(anchor.getText().contains("Edit Customer"))  {
		   
				WebElement editCust= driver.findElement(By.linkText("Edit Customer"));
		   
				Actions act= new Actions(driver);
		   
				act.moveToElement(editCust).doubleClick().build().perform();
	        
				break;
	       }
	    }
	 
	}catch (NoSuchElementException| StaleElementReferenceException e)  {
		
		List<WebElement> anchors = driver.findElements(By.tagName("a"));
		
		java.util.Iterator<WebElement> i = anchors.iterator();
		
		while(i.hasNext()) {
		
			WebElement anchor = i.next();                 
		    
			if(anchor.getText().contains("Edit Customer")) {
			 
				WebElement editCust= driver.findElement(By.linkText("Edit Customer"));
			   
				Actions act= new Actions(driver);
			   
				act.moveToElement(editCust).doubleClick().build().perform();
		      
				break;
		       }
		   }
	   }
    }

//******************************************Click GO button on CRM tab to search************************
public void Click_Go()
{
   try {
	   driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		
	   WebElement search=driver.findElement(By.xpath("//a[@id='P0_SEARCH']"));
	 	
	   Actions act= new Actions(driver);
	 	
	   act.moveToElement(search).doubleClick().build().perform();
	 	
   }catch(NoSuchElementException| StaleElementReferenceException e)	{
  			  
	   WebElement search=driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/table[1]/tbody[1]/tr[1]/td[7]/a[1]"));
	  	
	   Actions act= new Actions(driver);
	  	
	   act.moveToElement(search).doubleClick().build().perform();
   }
}
	
//************************************Method to Click Create customer*****************************
public void ClickCreateCustomer()
{
  try {
	
	  List<WebElement> anchors = driver.findElements(By.tagName("a"));
	
	  java.util.Iterator<WebElement> i = anchors.iterator();
    
	  while(i.hasNext()) {
    
		  WebElement anchor = i.next();                 
    
		  if(anchor.getText().contains("Create Customer")) {
	 
			  Actions act= new Actions(driver);
	   
			  act.moveToElement(anchor).click().build().perform();
            
			  break;
          }
       }
	
  }catch(StaleElementReferenceException e)	{ 
		
	  List<WebElement> anchors = driver.findElements(By.tagName("a"));
		
	  java.util.Iterator<WebElement> i = anchors.iterator();
	  
	  while(i.hasNext()) {
	  
		  WebElement anchor = i.next();                 
	    
		  if(anchor.getText().contains("Create Customer"))   {
		   
			  Actions act= new Actions(driver);
		   
			  act.moveToElement(anchor).click().build().perform();
	       
			  break;
	       }
	     }
	
  }catch(NoSuchElementException eE) { 		
		
	  List<WebElement> anchors = driver.findElements(By.tagName("a"));
		
	  java.util.Iterator<WebElement> i = anchors.iterator();
	  
	  while(i.hasNext()) {
	  
		  WebElement anchor = i.next();                 
	    
		  if(anchor.getText().contains("Create Customer"))  {
		  
			  Actions act= new Actions(driver);
		   
			  act.moveToElement(anchor).click().build().perform();
		      
			  break;
	       }
	    }
	}
  }

//**********************Method to test Access credit subscriber ******************************************	
public void AccessCreateSubcriber() throws InterruptedException
{
   try {

	   Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			   .withTimeout(30, TimeUnit.SECONDS) 			
			   .pollingEvery(1, TimeUnit.SECONDS) 			
			   .ignoring(NoSuchElementException.class);
   	   
	   WebElement Create_Subscriber= driver.findElement(By.id("B13989961185388418"));
		
	   Boolean his= Create_Subscriber.isDisplayed();
	   
	   wait.equals(his);
		
	   Actions act= new Actions(driver);
		
	   act.moveToElement(Create_Subscriber).doubleClick().build().perform();
		
   }catch (NoSuchElementException e)   {
			
	   WebElement Create_Subsciber= driver.findElement(By.id("B13989961185388418"));
		
	   Actions act= new Actions(driver);
		
	   act.moveToElement(Create_Subsciber).doubleClick().build().perform();  
	   
   }catch (StaleElementReferenceException E)  {
			  
	   WebElement Create_Subsciber= driver.findElement(By.id("B13989961185388418"));
		
	   Actions act= new Actions(driver);
		
	   act.moveToElement(Create_Subsciber).doubleClick().build().perform();
   }
	
   Thread.sleep(1500);
}


//*********************** Method to search number by MSISDN ********************************
public void Search_by_MSISDNnumberCRMTab(String msisdn)
{    
   try {
		  Select dropdown = new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));

		  dropdown.selectByVisibleText("MSISDN");
		  
		  driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
		  
		  driver.findElement(By.id("P0_SEARCH_VALUE")).sendKeys(msisdn);
		  
		  driver.findElement(By.id("P0_SEARCH")).submit();
		  
		  WebElement MSISDN= driver.findElement(By.id("P2_SI_MSISDN"));
		  
		  String MSIsdn= MSISDN.getText();
		  
		  Assert.assertEquals(MSIsdn, msisdn);
		  
		  driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
	  
   }catch(StaleElementReferenceException E)	  {
	  
	   Select dropdown = new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
	  
	   dropdown.selectByVisibleText("MSISDN");
	  
	   driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
	  
	   driver.findElement(By.id("P0_SEARCH_VALUE")).sendKeys(msisdn);
	  
	   driver.findElement(By.id("P0_SEARCH")).submit();
	  
	   WebElement MSISDN= driver.findElement(By.id("P2_SI_MSISDN"));
	  
	   String MSIsdn= MSISDN.getText();
	  
	   Assert.assertEquals(MSIsdn, msisdn);
	  
	   driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
	  
       }
   }

//*************************** Method to search by MSISDN ************************************
public void SearchByMSISDN(String msisdn)
{
   try {
		  Select dropdown = new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
		
		  dropdown.selectByVisibleText("MSISDN");
		  
		  driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
		  
		  driver.findElement(By.id("P0_SEARCH_VALUE")).sendKeys(msisdn);
		  
		  driver.findElement(By.id("P0_SEARCH")).submit();
		  
   }catch(StaleElementReferenceException E)   {
			   
	   Select dropdown = new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
		
	   dropdown.selectByVisibleText("MSISDN");
		
	   driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
		
	   driver.findElement(By.id("P0_SEARCH_VALUE")).sendKeys(msisdn);
		
	   driver.findElement(By.id("P0_SEARCH")).submit();
   }
}

//**************************** Method to click Billing tab *****************************
public void click_billingTab()
{
	WebElement billing=  driver.findElement(By.linkText("Billing"));

	Actions act= new Actions(driver);
	
	act.moveToElement(billing).click().build().perform();
	
}

//************************************** Method to apply filter and view filtered report **********************
public void view_filtered_report() throws InterruptedException
{
	try {	
		  // Click Action button
			
		click_ActionButton();
		
		Thread.sleep(300);

		//Click filter
		
		WebElement filter= driver.findElement(By.xpath("//a[@class='dhtmlSubMenuN'][contains(text(),'Filter')]"));
		
		Actions act = new Actions(driver);
		
		act.moveToElement(filter).click().build().perform();

		//Select Column
		
		Thread.sleep(300);
		
		Select column= new Select(driver.findElement(By.id("apexir_COLUMN_NAME")));
		
		column.selectByVisibleText(col);
		
		Thread.sleep(300);
		
		//select or enter expression

		WebElement expression= driver.findElement(By.id("apexir_EXPR"));
		
		expression.clear();
		
		expression.sendKeys(expvalue);
				
		//Click apply
		
		Thread.sleep(200);
		
		Click_apply();

	}catch(Exception e) {
		
		// Click Action button
		
		click_ActionButton();
		
		Thread.sleep(300);

		//Click filter
		
		WebElement filter= driver.findElement(By.xpath("//a[@class='dhtmlSubMenuN'][contains(text(),'Filter')]"));
		
		Actions act = new Actions(driver);
		
		act.moveToElement(filter).click().build().perform();

		//Select Column
		
		Thread.sleep(300);
		
		Select column= new Select(driver.findElement(By.id("apexir_COLUMN_NAME")));
		
		column.selectByVisibleText(col);
		
		Thread.sleep(300);

		//select or enter expression
		
		WebElement expression= driver.findElement(By.id("apexir_EXPR"));
		
		expression.clear();
		
		expression.sendKeys(expvalue);
		
		//Click apply
		
		Thread.sleep(200);
		
		Click_apply();
	}
}

//************************************* Method to save report *************************************
public void save_report() throws InterruptedException
{
	// Click Actions button

	click_ActionButton();
	
	// Click save report
	
	Thread.sleep(300);
	
	WebElement save= driver.findElement(By.xpath("//a[contains(text(),'Save Report')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(save).click().build().perform();
	 
	 //enter name of the report
	
	Thread.sleep(300);
	
	WebElement name= driver.findElement(By.id("apexir_WORKSHEET_NAME"));
	
	name.clear();
	
	name.sendKeys(reportname);
	 	 
	 // enter description
	
	Thread.sleep(300);
	
	WebElement description= driver.findElement(By.id("apexir_DESCRIPTION"));
	
	description.clear();
	
	description.sendKeys(reportname); 
	 	
	 //click apply
	
	Click_apply();
	
}

//**************************************** Method to search by private report ****************************
public void Search_by_private_report() throws InterruptedException
{
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));

	dropdown.selectByVisibleText(report);
}
		

//******************************** Method to select primary report *********************************
public void select_primary_report()
{
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText("1. Primary Report");
}


//******************************** Method to click action button ************************************
public void click_ActionButton()
{
	WebElement action= driver.findElement(By.id("apexir_ACTIONSMENUROOT"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(action).click().build().perform();
}

//************************************ Method to click apply ***************************************
public void Click_apply()
{
	WebElement apply= driver.findElement(By.id("apexir_btn_APPLY"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(apply).click().build().perform();
}

//*********************************** Method to test download CSV ***************************
public boolean Download_CSV(String downloadPath, String fileName) throws InterruptedException
{
	click_ActionButton();
	
	Thread.sleep(200);
	
	//Click download option
	
	WebElement reset = driver.findElement(By.xpath("//a[contains(text(),'Download')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(reset).click().build().perform();
	 
	// Choose and click CSV report type to download 
	
	Thread.sleep(1000);
	
	WebElement csv = driver.findElement(By.xpath("//a[@id='apexir_dl_CSV']//img"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(csv).click().build().perform();
	
	Thread.sleep(3000);
	
	File dir = new File(downloadPath);
	
	File[] dirContents = dir.listFiles();
	
	for (int i = 0; i < dirContents.length; i++) {
	
		if (dirContents[i].getName().equals(fileName)) {
	    
			// File has been found, it can now be deleted:
	    	
			Thread.sleep(1000);
	        
			dirContents[i].delete();
	        
			System.out.println("File is getting deleted");
	        
			return true;
	      }
		
	   }
	    
	return false;
	
}

//*********************************** Method to test download HTML ***************************
public boolean Download_HTML(String downloadPath, String fileName) throws InterruptedException
{

	Thread.sleep(200);
	
	// Choose and click HTML report type to download 
	
	Thread.sleep(500);
	
	WebElement html = driver.findElement(By.id("apexir_dl_HTML"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(html).click().build().perform();
	
	Thread.sleep(8000);
	
	File dir = new File(downloadPath);
	
	File[] dirContents = dir.listFiles();
	
	for (int i = 0; i < dirContents.length; i++) {
	
		if (dirContents[i].getName().equals(fileName)) {
	    
			// File has been found, it can now be deleted:
	    	
			Thread.sleep(1000);
	        
			dirContents[i].delete();
	        
			System.out.println("File is getting deleted");
	        
			return true;
	      }
   	}
	    
	return false;
}	

//*********************************** Method to send report via email *****************************
public void email_report() throws InterruptedException
{

	// Choose and click email report option 
	
	Thread.sleep(500);
	
	WebElement email = driver.findElement(By.id("apexir_dl_EMAIL"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(email).click().build().perform();
	 
	 // Enter email address of receiver	 
	
	WebElement TOemail= driver.findElement(By.id("apexir_EMAIL_TO"));
	
	TOemail.clear();
	
	TOemail.sendKeys(toemail);
	
	Thread.sleep(300);
	 
	 //Click send
	
	WebElement send = driver.findElement(By.id("apexir_btn_APPLY"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(send).click().build().perform();
}

//********************************* Method to filter report using flash back option ****************
public void flashback_filter() throws InterruptedException
{
	Thread.sleep(300);
	
	click_ActionButton();
	
	Thread.sleep(1000);
	
	//Click flash back option
	
	WebElement flashback= driver.findElement(By.xpath("//a[contains(text(),'Flashback')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(flashback).click().build().perform();
		
	// Enter flash back time
	
	Thread.sleep(800);
	
	WebElement time= driver.findElement(By.id("apexir_FLASHBACK_TIME"));
	
	time.clear();
	
	time.sendKeys(Time); 
	
	try{		
	
		//click apply
		
		Click_apply();
		
		Thread.sleep(5000);
		
		String text1= driver.findElement(By.xpath("//table[@id='apexir_CONTROL_PANEL_COMPLETE']")).getText(); 
		
		System.out.println(text1);
		
		Boolean successmsg= text1.endsWith(flashback_msg);
		
		Assert.assertTrue(successmsg);
		
	}catch(Exception e) {
		
		//click apply
	
		Click_apply();
		
		Thread.sleep(8000);
		
		String text1= driver.findElement(By.xpath("//table[@id='apexir_CONTROL_PANEL_COMPLETE']")).getText(); 
		
		System.out.println(text1);
		
		Boolean successmsg= text1.endsWith(flashback_msg);
		
		Assert.assertTrue(successmsg);
		
	}
}

//*************************** Method to click apply changes **************************
public void applyChanges()
{
	  WebElement applychanges = driver.findElement(By.id("B9584608921731998"));
	
	  Actions act= new Actions(driver);
	  
	  act.moveToElement(applychanges).click().build().perform();	
}

//*********************************** Method to test download HTML ***************************
public boolean Download_PDF(String downloadPath, String fileName) throws InterruptedException
{
	Thread.sleep(200);
	
	 // Choose and click PDF report type to download 
		
	Thread.sleep(500);
	
	WebElement pdf = driver.findElement(By.id("apexir_dl_PDF"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(pdf).click().build().perform();
	
	Thread.sleep(1500);
	
	File dir = new File(downloadPath);
	
	File[] dirContents = dir.listFiles();
	
	for (int i = 0; i < dirContents.length; i++) {
	
		if (dirContents[i].getName().equals(fileName)) {
	    
			// File has been found, it can now be deleted:
	    	
			Thread.sleep(1000);
	        
			dirContents[i].delete();
	        
			System.out.println("File is getting deleted");
	        
			return true;
	      }
		
	   	}
	      return false;
	  }	

//*********************************** Method to send report via email *****************************
public void get_subscription() throws InterruptedException
{
	WebElement chroller= driver.findElement(By.id("echo-page-header-toggler"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(chroller).click().build().perform();
	
	Thread.sleep(2500);
	
	click_ActionButton();
	
	Thread.sleep(500);
	
	 // Choose and click subscription option 
	
	Thread.sleep(500);
	
	WebElement email = driver.findElement(By.xpath("//a[contains(text(),'Subscription')]"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(email).click().build().perform();
	
	// Enter email address of receiver	 
	
	Thread.sleep(300);
	
	WebElement TOemail= driver.findElement(By.id("apexir_EMAIL_ADDRESS"));
	
	TOemail.clear();
	
	TOemail.sendKeys(toemail);
	 
	 //Click send
	
	WebElement send = driver.findElement(By.id("apexir_btn_APPLY"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(send).click().build().perform();
}


//*********************************** Method to test reset button on CIB ***********************************
public void Reset() throws InterruptedException
{

	Thread.sleep(200);
	
	click_ActionButton();
	
	Thread.sleep(500);
				
	//Click reset option
	
	WebElement reset = driver.findElement(By.xpath("//a[contains(text(),'Reset')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(reset).click().build().perform();	
	
	Thread.sleep(500);
					
	//click apply
	
	WebElement apply = driver.findElement(By.id("apexir_btn_APPLY"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(apply).click().build().perform();	
	
	Thread.sleep(800);
	
}

//**************************** Method to click Reporting tab *****************************
public void click_ReportingTab()
{

	WebElement reporting=  driver.findElement(By.linkText("Reporting"));

	Actions act= new Actions(driver);

	act.moveToElement(reporting).click().build().perform();

}

//**************************** Method to click workflow tab *****************************
public void click_WorkflowTab()
{
	WebElement Workflow=  driver.findElement(By.linkText("Workflow"));

	Actions act= new Actions(driver);
	
	act.moveToElement(Workflow).click().build().perform();
}


//************************* Method to check success message *************************
public void Success_Msg()
{

	String Msg= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[2]/div[1]")).getText();

	String Expected_msg= "Action Processed.";

	Assert.assertEquals(Msg, Expected_msg);
}

//***************************************** Method to click Sales Tab **********************
public void click_SalesTab()
{

	WebElement Sales=  driver.findElement(By.linkText("Sales"));

	Actions act= new Actions(driver);

	act.moveToElement(Sales).click().build().perform();
}

//********************************* Method to click ticketing tab ******************************
public void click_TicketingTab()
	{

	WebElement Ticketing =driver.findElement(By.linkText("Ticketing"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Ticketing).click().build().perform();
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
	
	}

//************************************ Method to click User admin tab *************************
public void click_UserAdmin()
{
	WebElement admin=  driver.findElement(By.linkText("User Admin"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(admin).click().build().perform();
	
	}
}
 

