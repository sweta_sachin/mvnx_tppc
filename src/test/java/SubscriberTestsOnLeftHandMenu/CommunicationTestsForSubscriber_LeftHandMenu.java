package SubscriberTestsOnLeftHandMenu;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import Common_Methods.CommonMethods;


public class CommunicationTestsForSubscriber_LeftHandMenu extends CommonMethods {
	
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
	}
	
	@AfterClass(alwaysRun=true)
	public void end()
	{
		driver.close();
	}
	
	@Test
	public void Click_Communication_ForSubscriber() throws InterruptedException
	{
		Click_communication();
	}
	
/////////////////////////////////////////////////////////////////////////////////////////////////
//******************************** Method to click bill limit history ***************************
public void Click_communication() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement attachment= driver.findElement(By.xpath("//div[@id='R21887018202990915']//a[contains(text(),'Communications')]"));
	
	Boolean name= attachment.isDisplayed();
	
	if(name==true)   {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(attachment).doubleClick().build().perform();
		
		Thread.sleep(1500);
		
		WebElement comm= driver.findElement(By.xpath("//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']"));
			
		Boolean COM= comm.isDisplayed();
		
		Assert.assertTrue(COM);
		}
	}
}
