package SubscriberTestsOnLeftHandMenu;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class EditSubscriberTests_FromLeftHandMenu extends Common_Methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
		driver.quit();			
	}
	
	@Test
	public void a_Click_Edit_Subscriber_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(2000);
		clickEditSubscriber();
	}
	
	@Test
	public void z_Edit_CustomerAddress_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(500);
		Edit_SubScriberAddress();
	}
	
	@Test
	public void c_View_Subscriber_Previous_Address_History_FromLeftHandMenu() throws InterruptedException
	{
		view_SubscriberAddressHistory();
	}
	
	@Test
	public void d_View_Subscriber_History_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(1000);
		View_subscriberHistory();
	}
	
/////////////////////////////////////////////////////////////////////////////////////////////
//*************************** Method to click edit subscriber *********************************
public void clickEditSubscriber()
{
  try {

	  WebElement editCust= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Edit Subscriber')]"));
	  
	  Actions act= new Actions(driver);
	  
	  act.moveToElement(editCust).click().build().perform();
	  
  }catch (NoSuchElementException| StaleElementReferenceException e)  {
	    	
	  WebElement editCust= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Edit Subscriber')]"));
	 
	  Actions act= new Actions(driver);
	 
	  act.moveToElement(editCust).click().build().perform();
  }
}

//****************************** Method to click create contact ******************************
public void click_create_contact()
{
  try {
	
	  List<WebElement> anchors = driver.findElements(By.tagName("a"));
		
	  java.util.Iterator<WebElement> i = anchors.iterator();
	  
	  while(i.hasNext()) {
	  
		  WebElement anchor = i.next();                 
	    
		  if(anchor.getText().contains("Create Contact"))   {
		   
			  WebElement editCust= driver.findElement(By.linkText("Create Contact"));
		   
			  Actions act= new Actions(driver);
		   
			  act.moveToElement(editCust).doubleClick().build().perform();
		  
			  break;
	       }
	    }
	  
  }catch (NoSuchElementException| StaleElementReferenceException e)   {
		  
	  List<WebElement> anchors = driver.findElements(By.tagName("a"));
		
	  java.util.Iterator<WebElement> i = anchors.iterator();
		
	  while(i.hasNext()) {
		
		  WebElement anchor = i.next();                 
		  
		  if(anchor.getText().contains("Create Contact"))   {
			  
			  WebElement editCust= driver.findElement(By.linkText("Create Contact"));
			  
			  Actions act= new Actions(driver);
			  
			  act.moveToElement(editCust).doubleClick().build().perform();
			  
			  break;
		       }
		   }
	   }
   }
//************************************** Method to edit customer address *********************
public void Edit_SubScriberAddress() throws InterruptedException
{ 

	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);

	WebElement add=   driver.findElement(By.linkText("Physical Address"));

	Boolean name1= add.isDisplayed();

	wait.equals(name1);

	add.click();

	Thread.sleep(1000);

	WebElement SecondLine=  driver.findElement(By.id("P34_ADDRESS_LINE1"));

	SecondLine.clear();

	SecondLine.sendKeys("123 ");

	driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

	try {

		WebElement save= driver.findElement(By.linkText("Save"));

		Actions act= new Actions(driver);

		act.moveToElement(save).doubleClick().build().perform();

	}catch(StaleElementReferenceException e){

		WebElement save= driver.findElement(By.linkText("Save"));

		Actions act= new Actions(driver);

		act.moveToElement(save).doubleClick().build().perform();
	}

	WebElement msg= driver.findElement(By.id("echo-message"));

	String successMSG= msg.getText();

	Boolean MSG= successMSG.endsWith("Successful");

	Assert.assertTrue(MSG);

}

//*********************************** Method to view subscriber previous address history **********
public void view_SubscriberAddressHistory() throws InterruptedException
{
  Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
		  .withTimeout(30, TimeUnit.SECONDS) 			
		  .pollingEvery(1, TimeUnit.SECONDS) 			
		  .ignoring(NoSuchElementException.class);

  WebElement Addhistory= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[3]/div[1]/div[1]/a[1]/span[1]"));
	
  Boolean ADDHis= Addhistory.isDisplayed();
	
  wait.equals(ADDHis);

  Actions act= new Actions(driver);

  Thread.sleep(1000);

  act.moveToElement(Addhistory).click().build().perform();

  Thread.sleep(1000);

  act.moveToElement(Addhistory).click().build().perform();


}
//*************************** Method to view subscriber history ***************************
public void View_subscriberHistory()
{

	try {

		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(1, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);
 
		WebElement history= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[4]/div[1]/div[1]/a[1]/span[1]"));
		
		Boolean his= history.isDisplayed();
		
		wait.equals(his);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(history).click().build().perform();
	
	}catch(NoSuchElementException e) {
	
		WebElement history= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[4]/div[1]/div[1]/a[1]/span[1]"));
	
		Actions act= new Actions(driver);
	
		act.moveToElement(history).click().build().perform();
	}
  }
}
