package SubscriberTestsOnLeftHandMenu;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class TransactionHistoryTests_LeftHandMenu  extends Common_Methods.CommonMethods{
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
	 driver.quit();
		
	}
	
	@Test
	public void a_Click_TransactionHistory_Subscriber()
	{
		click_transactionHistory();
	}
	
	@Test
	public void b_View_TransactionHistory_Subscriber()
	{
		 view_history();
	}
	
	
///////////////////////////////////////////////////////////////////////////////////////////////
//********************* Method to click transaction history **********************************
public void click_transactionHistory()
{
	WebElement editCust= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Transaction History')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(editCust).doubleClick().build().perform();
	
}
	
//******************************** Method to view transaction history ************************
public void view_history()
{
	WebElement history = driver.findElement(By.id("13433767776726456"));
	
	Boolean History= history.isDisplayed();
	
	Assert.assertTrue(History);
	
  }
}
