package SubscriberTestsOnLeftHandMenu;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class MSISDNSwap_FromLeftHandMenu extends Common_Methods.CommonMethods {
	
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
	}
	
	@AfterClass(alwaysRun=true)
	public void end()
	{
    	driver.close();
	}
	
	@Test
	public void a_Click_MSISDN_Swap_ForSubscriber_FromLeftHandMenu() throws InterruptedException
	{
		Click_MSISDNswap();
	}
	
	@Test
	public void b_Enter_New_MSISDN_ForSubscriber_FromLeftHandMenu() throws InterruptedException
	{
		EnterMSISDN();
	}
	
	@Test
	public void c_Click_Submit_ForMSISDNswap_ForSubscriber_FromLeftHandMenu() throws InterruptedException
	{
		ClickSubmit();
	}
	
	@Test
	public void d_Check_SuccessMsg_ForMSISDNSWAP_ForSubscriber_FromLeftHandMenu() throws InterruptedException
	{
		MSISDN_Swap_success();
	}
	
	@Test
	public void e_CheckRCSStatus_FromLeftHandMenu() throws InterruptedException
	{
		RCSStatus_ForMSISDNSwap();
	}
	
	
	
/////////////////////////////////////////////////////////////////////////////////////////////////
//******************************** Method to click MSISDN swap for a subscriber ***************************
public void Click_MSISDNswap() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement MSISDNSWAP= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'MSISDN Swap')]"));
	
	Boolean name= MSISDNSWAP.isDisplayed();
	
	if(name==true) {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(MSISDNSWAP).click().build().perform();
		
		Thread.sleep(1500);
		}
	}
		
//******************************* Method to enter new MSISDN into the textbox******************************
public void EnterMSISDN() throws InterruptedException
{
	driver.findElement(By.id("P48_NEW_MSISDN")).clear();
	
	driver.findElement(By.id("P48_NEW_MSISDN")).sendKeys(MSISDN);
	
	Thread.sleep(1500);
	
}
	
//******************************* Method to click submit ****************************************************
public void ClickSubmit() throws InterruptedException
{
	driver.findElement(By.id("B14240560088191304")).click();
	
	Thread.sleep(1500);
	
}

//******************************* Method to check success message after SIM swap is done ********************
public void MSISDN_Swap_success() throws InterruptedException
{
	Boolean MSISDNSWap= driver.findElement(By.xpath("//div[@id='active-notifications']//div[@class='ui-widget-header ui-corner-all ui-helper-clearfix echo-region-header']")).isDisplayed();	
	
	Assert.assertTrue(MSISDNSWap);
	
	Thread.sleep(1500);
	
}

//******************************* Method to check success message after SIM swap is done ********************
public void RCSStatus_ForMSISDNSwap() throws InterruptedException
{
	//Click RCS tab
	
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement RCS= driver.findElement(By.id("B16614757006394351"));
	
	Boolean name= RCS.isDisplayed();
	
	if(name==true)  {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(RCS).click().build().perform();
		
		Thread.sleep(1500);
		
		String RCS_Column = driver.findElement(By.xpath("//tr[2]//td[2]")).getText();
				
		if(RCS_Column=="Perform a MSISDN Swap")	{
				
			String Status= driver.findElement(By.xpath("//tr[2]//td[10]")).getText();
				
			Assert.assertEquals(Status, "Success");
		}
	} 
  }
}
