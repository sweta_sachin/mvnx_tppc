package SubscriberTestsOnLeftHandMenu;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Common_Methods.Attachmenttests;

public class AttachmentTests_Subscriber_LeftHandMenu extends Attachmenttests {
	
	

	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
	    clickAttachment();
	    Thread.sleep(1000);
	  	
	}
	
	@AfterClass(alwaysRun=true)
	public void end()
	{
		driver.close();
	}
	
	@Test
	public void a_Select_Document_Category_FOR_attachment_OnLefthandMenu() throws InterruptedException
	{
		Select_document_Category();
	}
	
	@Test
	public void b_Select_Document_Type_FOR_attachment_OnLefthandMenu() throws InterruptedException
	{
		Select_document_Type();
	}
	
	@Test
	public void c_Enter_name_FOR_attachment_OnLefthandMenu()
	{
		Enter_Name();
	}
	
	@Test
	public void d_Select_File_FOR_attachment_OnLefthandMenu() throws InterruptedException
	{
		
		Select_file();
	}
	
	@Test
	public void e_Click_Attach_FOR_attachment_OnLefthandMenu()
	{
		Click_attach();
	}
	
	@Test
	public void f_View_Attachments_FOR_attachment_OnLefthandMenu()
	{
		view_attachment();
	}
	
		
	@Test
	public void g_ClickEdit_attachment_FOR_attachment_OnLefthandMenu() throws InterruptedException
	{
		click_edit();
	} 
	
	@Test
	public void h_Edit_attach_new_File_FOR_attachment_OnLefthandMenu() throws InterruptedException
	{
		Attach_edit();
	}  
	
	
	@Test
	public void j_delete_attachment_FOR_attachment_OnLefthandMenu() throws InterruptedException
	{
		Delete_Attachment();
	}
	
	@Test
	public void i_Canceldelettion_FOR_attachment_OnLefthandMenu() throws InterruptedException
	{
		cancel_deletion_attachment();
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//***************************************** Method to click attachment *********************************
public void clickAttachment() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement attachment= driver.findElement(By.xpath("//div[@id='R21887018202990915']//a[contains(text(),'Attachments')]"));
	
	Boolean name= attachment.isDisplayed();
	
	if(name==true)   {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(attachment).doubleClick().build().perform();
		
		Thread.sleep(1500);
		}
	}
}
	