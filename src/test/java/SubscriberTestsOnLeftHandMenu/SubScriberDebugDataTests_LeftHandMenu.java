package SubscriberTestsOnLeftHandMenu;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Common_Methods.CommonMethods;


public class SubScriberDebugDataTests_LeftHandMenu  extends CommonMethods {
	
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
	}
	
	@AfterClass(alwaysRun=true)
	public void end()
	{
		driver.close();
	}
	
	@Test
	public void Click_Subscriber_DebugData_LeftHandMenu() throws InterruptedException
	{
		Click_debugdata();
	}
	
	@Test
	public void view_Subscriber_DebugData_LeftHandMenu() throws InterruptedException
	{
		view_debugdata();
	}
	
	
/////////////////////////////////////////////////////////////////////////////////////////////////
//******************************** Method to click bill limit history ***************************
public void Click_debugdata() throws InterruptedException
{
	WebElement attachment= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Subscriber Debug Data')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(attachment).doubleClick().build().perform();
	
	Thread.sleep(1500);
	
}

//****************************** View debug data ***************************************
public void view_debugdata() throws InterruptedException
{
	WebElement table= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[2]/div[1]"));
	
	Boolean report= table.isDisplayed();
	
	Assert.assertTrue(report);
	
  }
}
