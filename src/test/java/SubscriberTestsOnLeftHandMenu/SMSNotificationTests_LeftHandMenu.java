package SubscriberTestsOnLeftHandMenu;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import Common_Methods.CommonMethods;


public class SMSNotificationTests_LeftHandMenu extends CommonMethods {
	
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
	       	
	}
	
	@AfterClass(alwaysRun=true)
	public void end()
	{
		driver.close();
	}
	
	@Test
	public void Click_SMS_Notification_ForSubscriber_FromLeftHandMenu() throws InterruptedException
	{
		Click_notification();
	}
	
	
/////////////////////////////////////////////////////////////////////////////////////////////////
//*************************** Method to click SMS Notification ********************************
public void Click_notification() throws InterruptedException
{
	WebElement attachment= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'SMS Notifications')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(attachment).doubleClick().build().perform();
	
	Thread.sleep(1500);
		
  }
}
