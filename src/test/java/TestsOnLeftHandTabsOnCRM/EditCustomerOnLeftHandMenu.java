package TestsOnLeftHandTabsOnCRM;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class EditCustomerOnLeftHandMenu  extends Common_Methods.CommonMethods {
	
	 public static String Street= "Old street";
		
		public static String CODE= "12345";
		
		public static String editName= "new name";
	
		@BeforeClass()
	public void CRM()
	{
		Login();
		SearchCustomerByAccountNumber(AccNo);
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
		driver.manage().timeouts().implicitlyWait(2,TimeUnit.SECONDS);
		driver.quit();
	}
	
	@Test
	public void a_editCustomerEditName_OnLeftHand_MenuBar() throws Exception
	{
		Thread.sleep(800);
		Edit_CustomerName();
	}
	
	@Test
	public void z_editCustomerEditAddress_OnLeftHand_MenuBar() throws InterruptedException
	{
		Edit_CustomerAddress();
		System.out.println(driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[2]/div[1]")).getText());
	}  
	
	@Test
	public void viewCustomerHistory_OnLeftHand_MenuBar() throws Throwable
	{   		
		ViewCustomerHistory();
	}
	
	@Test
	public void b_viewCustomerAddressHistory_OnLeftHand_MenuBar() throws Throwable
	{   
		ViewAddressHistory();
	}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//************************************** Method to edit customer name ********************************
public void Edit_CustomerName() throws Exception
{
	Clickedit_Customer_Tab();
	
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(1, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement F_name= driver.findElement(By.id("P40_BCD_NAME"));
	
	Boolean name= F_name.isDisplayed();
	
	wait.equals(name);
	
	F_name.clear();
	
	F_name.sendKeys(editName);
	
	driver.findElement(By.id("P40_BCD_TYPE_2")).click();
	
	driver.findElement(By.id("P40_BCD_SURNAME")).clear();
	
	driver.findElement(By.id("P40_BCD_SURNAME")).sendKeys("surname");
	
	Thread.sleep(1000);
	
	driver.findElement(By.id("B13363654567673223")).click();
	
	WebElement SuccessMsg= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[2]/div[1]"));
	
	Boolean name1= SuccessMsg.isDisplayed();
	
	Assert.assertTrue(name1);
		
	WebElement NameOnFE= driver.findElement(By.id("P2_BCD_NAME"));
	
	String nameonFE= NameOnFE.getText();
	
	System.out.println(nameonFE);
		
	Assert.assertEquals(nameonFE, editName);
	
	Thread.sleep(1000);
		 
}

//***********************************Method to view customer history ********************************
public void ViewCustomerHistory() throws InterruptedException 
{ 
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(1, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
	
	WebElement history= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[5]/div[1]/div[1]/a[1]/span[1]"));
	
	Boolean his= history.isDisplayed();
	
	wait.equals(his);
	
	Actions act= new Actions(driver);
	
	act.moveToElement(history).click().build().perform();
	
	Thread.sleep(1000);

}

//**************************************Method to edit customer address ****************************  
public void Edit_CustomerAddress() throws InterruptedException
{ 
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
	
	WebElement add=   driver.findElement(By.linkText("Physical Address"));
	
	Boolean name1= add.isDisplayed();
	
	wait.equals(name1);
	
	add.click();
	
	Thread.sleep(1200);
	
	WebElement SecondLine=driver.findElement(By.xpath("//input[@id='P40_ADDRESS_LINE2']"));
	
	SecondLine.clear();
	
	SecondLine.sendKeys(Street);
	
	WebElement code=driver.findElement(By.xpath("//input[@id='P40_ADDRESS_LINE5']"));
	
	code.clear();
	
	code.sendKeys(CODE);

	
	driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
	
	try {
	
		WebElement save= driver.findElement(By.linkText("Save"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(save).doubleClick().build().perform();
		
	}catch(StaleElementReferenceException e){
					
		WebElement save= driver.findElement(By.linkText("Save"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(save).doubleClick().build().perform();
		
	}

	String Address= driver.findElement(By.xpath("//div[@id='R13420071199266104']//tr[3]//td[3]")).getText();
	
	System.out.println(Address);
	
	Assert.assertEquals(Address, Street);
	
	String cod= driver.findElement(By.xpath("//div[@id='R13420071199266104']//tr[3]//td[6]")).getText();
	
	System.out.println(cod);
	
	Assert.assertEquals(cod, CODE);
}

//********************************Method to view address history ***********************************	  
public void ViewAddressHistory() throws Throwable
{
	ClickEdit_Customer();
	
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(1, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
		   
	WebElement Addhistory= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[4]/div[1]/div[1]/a[1]/span[1]"));
	
	Boolean ADDHis= Addhistory.isDisplayed();
	
	wait.equals(ADDHis);
	
	Actions act= new Actions(driver);
	
	Thread.sleep(1000);
	
	act.moveToElement(Addhistory).click().build().perform();
	
	WebElement ad= driver.findElement(By.xpath("//div[@id='R13422163627292273']//tr[2]//td[1]"));
	
	Boolean AD= ad.isDisplayed();
	
	Assert.assertTrue(AD);
	
	WebElement footer= driver.findElement(By.xpath("//div[@id='R13422163627292273']//div[@class='echo-region-buttons bottom clearfix']"));
	
	act.moveToElement(footer).click().build().perform();   
}

//****************************** Method to click edit customer **********************************
public void Clickedit_Customer_Tab()
{
	try {
	
		WebElement editCust= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/ul[1]/li[6]/a[1]"));
		       
		Actions act= new Actions(driver);
		
		act.moveToElement(editCust).doubleClick().build().perform();
			
	}catch (NoSuchElementException| StaleElementReferenceException e)    {
			  
		WebElement editCust= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/ul[1]/li[6]/a[1]"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(editCust).doubleClick().build().perform();
    	}
    }
}
