package TestsOnLeftHandTabsOnCRM;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Common_Methods.Attachmenttests;



public class AttachmentOnLeftHandMenu extends Attachmenttests {
	
	

	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		SearchCustomerByAccountNumber(AccNo);
		Thread.sleep(1000);;
	    clickAttachment();
	  	
	}
	
	@AfterClass(alwaysRun=true)
	public void end()
	{
		driver.close();
	}
	
	@Test
	public void a_Select_Document_Category_TestFromLeftHandMenu() throws InterruptedException
	{
		Select_document_Category();
	}
	
	@Test
	public void b_Select_Document_Type_TestFromLeftHandMenu() throws InterruptedException
	{
		Select_document_Type();
	}
	
	@Test
	public void c_Enter_name_TestFromLeftHandMenu()
	{
		Enter_Name();
	}
	
	@Test
	public void d_Select_File_TestFromLeftHandMenu() throws InterruptedException
	{
		Select_file();
	}
	
	@Test
	public void e_Click_Attach_TestFromLeftHandMenu()
	{
		Click_attach();
	}
	
	@Test
	public void f_View_Attachments_TestFromLeftHandMenu()
	{
		view_attachment();
	}
	
		
	@Test
	public void g_ClickEdit_attachment_TestFromLeftHandMenu() throws InterruptedException
	{
		click_edit();
	} 
	
	@Test
	public void h_Edit_attach_new_File_TestFromLeftHandMenu() throws InterruptedException
	{
		Attach_edit();
	}  
	
	
	@Test
	public void j_delete_attachment_TestFromLeftHandMenu() throws InterruptedException
	{
		Delete_Attachment();
	}
	
	@Test
	public void i_Canceldelettion_TestFromLeftHandMenu() throws InterruptedException
	{
		cancel_deletion_attachment();
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//*********************************** Method to click attachment ******************************************
public void clickAttachment() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
			
	WebElement attachment= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/ul[1]/li[1]/a[1]"));
	
	Boolean name= attachment.isDisplayed();
	
	if(name==true)   {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(attachment).doubleClick().build().perform();
		
		Thread.sleep(1500);
		}
	}
}
